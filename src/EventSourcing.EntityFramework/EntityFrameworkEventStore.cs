using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Avenue.EventSourcing.EntityFramework
{
    public abstract class EntityFrameworkEventStore<TEventEntity, TDomainEvent> : IEventStore<TDomainEvent>
        where TEventEntity : class, IEventEntity
    {
        protected abstract DbSet<TEventEntity> GetEntityEventSet();

        protected abstract TEventEntity CreateEventEntity(Type aggregateRootType, Guid aggregateRootId,
            object @event);

        public async Task<IEnumerable<TDomainEvent>> Get<TAggregateRoot>(Guid aggregateId)
        {
            var set = GetEntityEventSet();
            var eventEntities = set.AsEnumerable()
                .Where(x=>x.AggregateType.ToString() == typeof(TAggregateRoot).ToString() && x.AggregateId == aggregateId);

            List<TDomainEvent> events = eventEntities
                .OrderBy(x => x.GetSequencer())
                .Select(x => x.GetEvent())
                .Where(x=> x is TDomainEvent)
                .Cast<TDomainEvent>().ToList();
            return events;
        }

        public Task Write<TAggregateRoot>(Guid aggregateId, IEnumerable<TDomainEvent> @event)
        {
            var set = GetEntityEventSet();
            var eventEntities = @event.Select(x => CreateEventEntity(typeof(TAggregateRoot), aggregateId, x)).ToList();
            set.AddRange(eventEntities);
            return Task.CompletedTask;
        }
    }
}