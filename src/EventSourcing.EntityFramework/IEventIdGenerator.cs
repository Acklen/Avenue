using System;

namespace Avenue.EventSourcing.EntityFramework
{
    public interface IEventIdGenerator
    {
        Guid Generate();
    }
}