using System;
using System.Runtime.Serialization;

namespace Avenue.EventSourcing.EntityFramework
{
    public class AggregateEvent : IEventEntity
    {
        protected AggregateEvent()
        {
        }

        public AggregateEvent(Guid eventId, Type aggregateType, Guid aggregateId, object @event, DateTime published,
            Guid actionSetId)
        {
            Id = eventId;
            AggregateType = aggregateType;
            AggregateId = aggregateId;
            EventData = @event;
            EventType = @event.GetType();
            Published = published;
            ActionSetId = actionSetId;
        }

        public virtual Guid Id { get; protected set; }

        public virtual object GetEvent()
        {
            return EventData;
        }

        public virtual object GetSequencer()
        {
            return Published.Ticks;
        }

        public virtual Type AggregateType { get; protected set; }

        public virtual Guid AggregateId { get; protected set; }

        public virtual object EventData { get; protected set; }
        public virtual Type EventType { get; protected set; }

        public virtual DateTime Published { get; protected set; }
        public virtual Guid ActionSetId { get; protected set; }
    }
}