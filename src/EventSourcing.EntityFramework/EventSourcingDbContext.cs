using System;
using Avenue.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Newtonsoft.Json;
using TypeScanning;

namespace Avenue.EventSourcing.EntityFramework
{
    public class EventSourcingDbContext : DbContext, IDbContext
    {
        public EventSourcingDbContext(DbContextOptions<EventSourcingDbContext> options) : base(options)
        {
        }


        public DbSet<AggregateEvent> Events { get; protected set; }

        public Guid TransactionId => Database.CurrentTransaction.TransactionId;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var serializerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                TypeNameHandling = TypeNameHandling.Objects
            };

            var entityTypeBuilder = modelBuilder
                .Entity<AggregateEvent>();

            entityTypeBuilder
                .Property(e => e.EventData).HasConversion(
                    obj => JsonConvert.SerializeObject(obj, serializerSettings),
                    json => JsonConvert.DeserializeObject(json, serializerSettings));

            var typeScanner = new TypeScanner();
            entityTypeBuilder.Property(x => x.AggregateType).HasConversion(
                type => type.FullName,
                typeName => typeScanner.GetOneType(typeName));

            entityTypeBuilder.Property(x => x.EventType).HasConversion(
                type => type.FullName,
                typeName => typeScanner.GetOneType(typeName));
        }
    }
}