using System;

namespace Avenue.EventSourcing.EntityFramework
{
    public class EventIdGenerator : IEventIdGenerator
    {
        public Guid Generate()
        {
            return Guid.NewGuid();
        }
    }
}