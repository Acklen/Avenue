using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avenue.Domain;

namespace Avenue.EventSourcing.EntityFramework
{
    public class EventSourcedWritableRepository<TAggregateRoot> : IWritableRepository<TAggregateRoot> where TAggregateRoot : class, IAggregateRoot
    {
        readonly IEventStore<IDomainEvent> _eventStore;

        public EventSourcedWritableRepository(IEventStore<IDomainEvent> eventStore)
        {
            _eventStore = eventStore;
        }

        TAggregateRoot BuildAggregateRoot(IEnumerable<IDomainEvent> events)
        {
            try
            {
                var instance = Activator.CreateInstance(typeof(TAggregateRoot), events);
                return (TAggregateRoot) instance;
            }
            catch (Exception e)
            {
                throw new AggregateRootInstantiationException<TAggregateRoot>(e);
            }
        }

        public async Task<TAggregateRoot> Find(Guid id)
        {
            IEnumerable<IDomainEvent> events = await _eventStore.Get<TAggregateRoot>(id);
            return BuildAggregateRoot(events);
        }

        public async Task<TAggregateRoot> Create(TAggregateRoot entity)
        {
            return await PersistEvents(entity);
        }

        public async Task<TAggregateRoot> Update(TAggregateRoot entity)
        {
            return await PersistEvents(entity);
        }

        async Task<TAggregateRoot> PersistEvents(TAggregateRoot entity)
        {
            List<IDomainEvent> changes = entity.GetChanges().ToList();
            await _eventStore.Write<TAggregateRoot>(entity.Id, changes);
            entity.ClearChanges();
            return entity;
        }
    }
}