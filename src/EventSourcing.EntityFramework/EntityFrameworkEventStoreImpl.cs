using System;
using Avenue.Domain;
using Avenue.Events;
using Avenue.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Avenue.EventSourcing.EntityFramework
{
    public class EntityFrameworkEventStoreImpl<TDomainEvent> : EntityFrameworkEventStore<AggregateEvent, TDomainEvent>
    {
        readonly IDbContext _dbContext;
        readonly ITimeProvider _clock;
        readonly IEventSourcingActionSetIdGenerator _actionSetIdGenerator;
        readonly IEventIdGenerator _eventIdGenerator;

        public EntityFrameworkEventStoreImpl(IDbContext dbContext, ITimeProvider clock,
            IEventSourcingActionSetIdGenerator actionSetIdGenerator, IEventIdGenerator eventIdGenerator)
        {
            _dbContext = dbContext;
            _clock = clock;
            _actionSetIdGenerator = actionSetIdGenerator;
            _eventIdGenerator = eventIdGenerator;
        }

        protected override DbSet<AggregateEvent> GetEntityEventSet()
        {
            return _dbContext.Set<AggregateEvent>();
        }

        protected override AggregateEvent CreateEventEntity(Type aggregateRootType, Guid aggregateRootId, object @event)
        {
            var id = _eventIdGenerator.Generate();
            return new AggregateEvent(id, aggregateRootType, aggregateRootId, @event, _clock.GetNow(),
                _actionSetIdGenerator.Generate(_dbContext));
        }
    }
}