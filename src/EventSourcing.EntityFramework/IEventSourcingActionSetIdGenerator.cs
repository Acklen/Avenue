using System;
using Avenue.Persistence.EntityFramework;

namespace Avenue.EventSourcing.EntityFramework
{
    public interface IEventSourcingActionSetIdGenerator
    {
        Guid Generate(IDbContext dbContext);
    }
}