using System;
using Avenue.Persistence.EntityFramework;

namespace Avenue.EventSourcing.EntityFramework
{
    public class EventSourcingActionSetIdGenerator : IEventSourcingActionSetIdGenerator
    {
        public Guid Generate(IDbContext dbContext)
        {
            return dbContext.TransactionId;
        }
    }
}