using System;

namespace Avenue.EventSourcing.EntityFramework
{
    public class AggregateRootInstantiationException<T> : Exception
    {
        public AggregateRootInstantiationException(Exception innerException)
            : base(
                $"Unable to instantiate the {typeof(T)} because there was no constructor that accepts a collection of objects (domain events). See inner exception for details.",
                innerException)
        {
        }
    }
}