using System;

namespace Avenue.EventSourcing.EntityFramework
{
    public interface IEventEntity
    {
        Type AggregateType { get; }
        Guid AggregateId { get; }
        object GetEvent();
        object GetSequencer();
    }
}