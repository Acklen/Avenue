using System.Collections.Generic;
using Avenue.EventSourcing;
using EventSourcing;

namespace EventSourcing.InMemoryStore.Specs
{
    public class Horse : AggregateRoot
    {
        public Horse(IEnumerable<string> domainEvents) : base(domainEvents)
        {
        }
    }
}