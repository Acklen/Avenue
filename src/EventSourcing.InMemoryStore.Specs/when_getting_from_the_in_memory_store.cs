using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Avenue.Events;
using Avenue.EventSourcing.InMemoryStore;
using EventSourcing.InMemoryStore;
using Machine.Specifications;
using Xunit;

namespace EventSourcing.InMemoryStore.Specs
{
    public class when_getting_from_the_in_memory_store
    {
        Establish _context = () =>
        {
            _systemUnderTest = new InMemoryEventStore<string>();
            _aggregateId = Guid.NewGuid();
            _eventWrappers = new List<EventWrapper>
            {
                new EventWrapper(typeof(Horse), _aggregateId, "test", DateTime.Now)
            };

            _systemUnderTest.Events.AddRange(_eventWrappers);
        };

        Because of = async () => { _result = await _systemUnderTest.Get<Horse>(_aggregateId); };

        It should_do_retrieve_the_associated_events = () => { _result.ShouldBeLike(new[] {"test"}); };

        Cleanup after = () => _systemUnderTest.ClearAll();

        static InMemoryEventStore<string> _systemUnderTest;
        static List<EventWrapper> _eventWrappers;
        static Guid _aggregateId;
        static IEnumerable<object> _result;
    }
}