using System;
using System.Collections.Generic;
using System.Linq;
using Avenue.EventSourcing.InMemoryStore;
using EventSourcing.InMemoryStore;
using Machine.Specifications;

namespace EventSourcing.InMemoryStore.Specs
{
    public class when_writing_to_the_in_memory_store
    {
        Establish _context = () =>
        {
            _systemUnderTest = new InMemoryEventStore<string>();
            _aggregateId = Guid.NewGuid();

            _eventsToWrite = new List<string> {"test1", "test2"};
        };

        Because of = async () => { await _systemUnderTest.Write<Horse>(_aggregateId, _eventsToWrite); };

        It should_do_retrieve_the_associated_events = () =>
            _systemUnderTest.Events.ToList().Select(x => x.Event).ShouldBeLike(_eventsToWrite);

        static InMemoryEventStore<string> _systemUnderTest;
        static Guid _aggregateId;
        static List<string> _eventsToWrite;

        Cleanup after = () => _systemUnderTest.ClearAll();
    }
}