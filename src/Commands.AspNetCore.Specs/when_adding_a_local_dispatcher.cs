using Avenue.AspNetCore;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.Testing;
using Avenue.Testing.AspNetCore;
using FluentAssertions;
using Machine.Specifications;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using It = Machine.Specifications.It;

namespace Commands.AspNetCore.Specs
{
    public class when_adding_a_local_dispatcher
    {
        Establish _context = () =>
        {
            _services = new ServiceCollection();
            _commandQueue = Mock.Of<IPublisher<ICommand>>();
        };

        Because of = () =>
        {
            _services.AddLocalCommandDispatcher(_commandQueue);
        };

        It should_resolve_a_queued_command_dispatcher = () =>
            _services.ShouldResolve<IDispatcher<ICommand>>()
                .ThatIsA<QueuedCommandDispatcherDecorator>();

        It should_resolve_a_command_queue = () =>
            _services.ShouldResolve<IPublisher<ICommand>>()
                .Should().Be(_commandQueue);
            
        static ServiceCollection _services;
        static IPublisher<ICommand> _commandQueue;
    }
}