using Avenue.AspNetCore;
using FluentAssertions;
using Machine.Specifications;
using Microsoft.Extensions.DependencyInjection;

namespace Commands.AspNetCore.Specs
{
    public class when_creating_a_service_collection
    {
        Establish _context = () => { _systemUnderTest = new ServiceCollectionFactory() as IServiceCollectionFactory; };

        Because of = () => { _result = _systemUnderTest.Create(); };

        It should_return_a_service_collection = () => { _result.Should().NotBeNull(); };
        static IServiceCollectionFactory _systemUnderTest;
        static IServiceCollection _result;
    }
}