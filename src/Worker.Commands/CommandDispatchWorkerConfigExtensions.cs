using System;
using System.Reflection;
using Avenue.AspNetCore.Extensions;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Domain;
using Microsoft.Extensions.DependencyInjection;
using Worker.AspNetCore;

namespace Avenue.Worker.Commands
{
    public static class CommandDispatchWorkerConfigExtensions
    {
        public static DispatchWorkerConfig<ICommand> WithCommandHandlersFrom(this DispatchWorkerConfig<ICommand> config,
            params Assembly[] assemblies)
        {
            config.WithDependencies(s =>
            {
                s.Decorate<IDispatcher<ICommand>, CommandHandlingDispatcherDecorator>();
                s.Scan<ICommandHandler>(assemblies);
            });
            return config;
        }

        public static DispatchWorkerConfig<ICommand> WithCommandValidatorsFrom(
            this DispatchWorkerConfig<ICommand> config, params Assembly[] assemblies)
        {
            config.WithDependencies(s =>
            {
                s.Decorate<IDispatcher<ICommand>, ValidatingCommandDispatcher>();
                s.Scan<ICommandValidator>(assemblies);
            });
            return config;
        }

        public static DispatchWorkerConfig<ICommand> WithPersistenceConfiguration(
            this DispatchWorkerConfig<ICommand> config, Action<PersistenceConfig> persistenceConfigAction)
        {
            var persistenceConfig = new PersistenceConfig();
            persistenceConfigAction.Invoke(persistenceConfig);
            config.WithDependencies(s => persistenceConfig.Apply(s));
            return config;
        }

        public static IServiceCollection WithPersistenceConfiguration(
            this IServiceCollection services, Action<PersistenceConfig> persistenceConfigAction)
        {
            var persistenceConfig = new PersistenceConfig();
            persistenceConfigAction.Invoke(persistenceConfig);
            persistenceConfig.Apply(services);
            return services;
        }
    }
}