using Avenue.Commands;
using Avenue.Commands.EntityFramework;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.EventSourcing;
using Avenue.EventSourcing.EntityFramework;
using Avenue.Persistence.EntityFramework;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.Worker.Commands
{
    public static class CommandPersistenceConfigExtensions
    {
        public static PersistenceConfig WithCommandUnitOfWork(this PersistenceConfig config)
        {
            config.ServiceConfigurations.Enqueue(x => x
                .AddTransient<IUnitOfWork, DefaultUnitOfWork>());

            config.ServiceConfigurations.Enqueue(x =>
                x.Decorate<IDispatcher<ICommand>, UnitOfWorkCommandDispatcherDecorator>());

            return config;
        }
        
        public static PersistenceConfig WithWritableAggregateRootRepository<TAggregateRootType>(this PersistenceConfig config) where TAggregateRootType : class, IAggregateRoot
        {
            config.WithEntityTypes(typeof(TAggregateRootType));
            
            config.ServiceConfigurations.Enqueue(x=> x
                .AddTransient<IEventSourcingActionSetIdGenerator, EventSourcingActionSetIdGenerator>()
                .AddTransient<IEventIdGenerator, EventIdGenerator>()
                .AddTransient<ITimeProvider, UtcTimeProvider>()
                .AddTransient(typeof(IEventStore<IDomainEvent>),
                    typeof(EntityFrameworkEventStoreImpl<IDomainEvent>))
                .AddTransient(typeof(IWritableRepository<TAggregateRootType>),
                    typeof(EventSourcedWritableRepository<TAggregateRootType>)));
            return config;
        }

    }
}