using System.Threading.Tasks;
using Avenue.Dispatch;

namespace Avenue.Events
{
    public interface IEventHandler : IHandler
    {}

    public interface IEventHandler<in TEvent> : IEventHandler
    {
        Task Handle(TEvent @event);
    }
}