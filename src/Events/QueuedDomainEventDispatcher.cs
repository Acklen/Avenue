using System.ComponentModel;
using System.Threading.Tasks;
using Avenue.Dispatch;
using Avenue.Domain;

namespace Avenue.Events
{
    public class QueuedDomainEventDispatcher : IDispatcher<IDomainEvent>
    {
        readonly IPublisher<IDomainEvent> _publisher;

        public QueuedDomainEventDispatcher(IPublisher<IDomainEvent> publisher)
        {
            _publisher = publisher;
        }

        public async Task Dispatch(IDomainEvent commandOrEvent)
        {
            await _publisher.Publish(commandOrEvent);
        }
    }
}