using System.Threading.Tasks;
using Avenue.Dispatch;
using Avenue.Domain;

namespace Avenue.Events
{
    public class UnitOfWorkEventDispatcherDecorator : IDispatcher<IDomainEvent>
    {
        readonly IUnitOfWork _unitOfWork;

        public UnitOfWorkEventDispatcherDecorator(IDispatcher<IDomainEvent> decorated, IUnitOfWork unitOfWork)
        {
            DecoratedDispatcher = decorated;
            _unitOfWork = unitOfWork;
        }

        public IDispatcher<IDomainEvent> DecoratedDispatcher { get; }

        public Task Dispatch(IDomainEvent commandOrEvent)
        {
            return _unitOfWork.Execute(async () => await DecoratedDispatcher.Dispatch(commandOrEvent));
        }
    }
}