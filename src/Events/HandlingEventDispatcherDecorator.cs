using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avenue.Dispatch;
using Avenue.Domain;

namespace Avenue.Events
{
    public class HandlingEventDispatcherDecorator : IDispatcher<IDomainEvent>
    {
        readonly IDispatcher<IDomainEvent> _decoratedDispatcher;
        readonly IDispatcher<IDomainEvent> _dispatcher;

        public HandlingEventDispatcherDecorator(IDispatcher<IDomainEvent> decoratedDispatcher, IEnumerable<IEventHandler> eventHandlers)
        {
            _decoratedDispatcher = decoratedDispatcher;
            _dispatcher = new Dispatcher<IDomainEvent>(new DefaultHandlerMatcher(eventHandlers.Select(x => (IHandler) x)),
                new EventDispatcherConfig()
            );
        }

        public async Task Dispatch(IDomainEvent commandOrEvent)
        {
            await _decoratedDispatcher.Dispatch(commandOrEvent);
            await _dispatcher.Dispatch(commandOrEvent);
        }
    }
}