using Avenue.Dispatch;

namespace Avenue.Events
{
    public class EventDispatcherConfig : IDispatcherConfig
    {
        public int MaximumHandlersPerDispatch => 10000;
        public int MinimumHandlersPerDispatch => 0;
    }
}