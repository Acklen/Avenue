using System.Windows.Input;
using Avenue.AspNetCore.Extensions;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.Events;
using Avenue.Persistence.EntityFramework;
using Avenue.Persistence.Relational;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.Worker.Events
{
    public static class EventPersistenceConfigExtensions
    {
        public static PersistenceConfig WithDomainEventDispatch(this PersistenceConfig config, IPublisher<IDomainEvent> publisher)
        {
            var serviceOpen = typeof(IWritableRepository<>);
            var implOpen = typeof(DispatchingWritableRepository<>);
            config.ServiceConfigurations.Enqueue(x => { x.DecorateForEachType(serviceOpen, implOpen, config.EntityTypes); });
            
            config.ServiceConfigurations.Enqueue(x => x.AddTransient<IDispatcher<IDomainEvent>, QueuedDomainEventDispatcher>()
                .AddSingleton(publisher));
            
            return config;
        }
        
        public static PersistenceConfig WithDomainEventUnitOfWork(this PersistenceConfig config)
        {
            config.ServiceConfigurations.Enqueue(x => x
                .AddTransient<IUnitOfWork, DefaultUnitOfWork>());

            config.ServiceConfigurations.Enqueue(x =>
                x.Decorate<IDispatcher<IDomainEvent>, UnitOfWorkEventDispatcherDecorator>());

            return config;
        }
    }
}