using System;
using System.Reflection;
using Avenue.AspNetCore.Extensions;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.Events;
using Microsoft.Extensions.DependencyInjection;
using Worker.AspNetCore;

namespace Avenue.Worker.Events
{
    public static class EventDispatchWorkerConfigExtensions
    {
        public static DispatchWorkerConfig<IDomainEvent> WithEventHandlersFrom(
            this DispatchWorkerConfig<IDomainEvent> config, params Assembly[] assemblies)
        {
            config.WithDependencies(s =>
            {
                s.Decorate<IDispatcher<IDomainEvent>, HandlingEventDispatcherDecorator>();
                s.Scan<IEventHandler>(assemblies);
            });
            return config;
        }
        
        public static DispatchWorkerConfig<IDomainEvent> WithPersistenceConfiguration(
            this DispatchWorkerConfig<IDomainEvent> config, Action<PersistenceConfig> persistenceConfigAction)
        {
            var persistenceConfig = new PersistenceConfig();
            persistenceConfigAction.Invoke(persistenceConfig);
            config.WithDependencies(s => persistenceConfig.Apply(s));
            return config;
        }
    }
}