using System;
using System.Collections.Generic;
using System.Linq;
using Avenue.Events;
using Machine.Specifications;

namespace EventSourcing.EntityFramework.Specs.Integration
{
    public class when_writing_a_stream_of_events
    {
        Establish _context = () =>
        {
            _systemUnderTest = new TestEventStore();
            _aggregateId = Guid.NewGuid();
        };

        Because of = () =>
        {
            _systemUnderTest.Write<Monkey>(_aggregateId, new List<string> {"monkey", "horse"}).Await();
            _systemUnderTest.DbContext.SaveChanges();
        };

        It should_persist_the_first_event = () =>
        {
            _systemUnderTest.DbContext.Events.First(x => x.Sequence == 1).Event.ShouldEqual("monkey");
        };

        It should_persist_the_second_event = () =>
        {
            _systemUnderTest.DbContext.Events.First(x => x.Sequence == 2).Event.ShouldEqual("horse");
        };

        Cleanup after = () => { _systemUnderTest.DbContext.Database.EnsureDeleted(); };

        static TestEventStore _systemUnderTest;
        static Guid _aggregateId;
    }
}