using System;
using System.Collections.Generic;
using Avenue.Events;
using Avenue.EventSourcing.EntityFramework;
using EventSourcing.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace EventSourcing.EntityFramework.Specs.Integration
{
    internal class TestEventStore : EntityFrameworkEventStore<TestEventEntity, string>
    {
        int _count;

        public TestDbContext DbContext { get; }

        public TestEventStore()
        {
            _count = 0;
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase("test")
                .Options;

            DbContext = new TestDbContext(options);
        }

        public TestEventStore Seed(IEnumerable<TestEventEntity> events)
        {
            DbContext.Events.AddRange(events);
            DbContext.SaveChanges();
            Console.WriteLine(events);
            return this;
        }

        protected override DbSet<TestEventEntity> GetEntityEventSet()
        {
            return DbContext.Set<TestEventEntity>();
        }

        protected override TestEventEntity CreateEventEntity(Type aggregateRootType, Guid aggregateRootId,
            object @event)
        {
            _count++;
            return new TestEventEntity(aggregateRootType, aggregateRootId, @event.ToString(), _count);
        }
    }
}