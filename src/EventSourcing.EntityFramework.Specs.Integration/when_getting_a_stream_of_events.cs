using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Machine.Specifications;

namespace EventSourcing.EntityFramework.Specs.Integration
{
    public class when_getting_a_stream_of_events
    {
        Establish _context = () =>
        {
            _aggregateId = Guid.Empty;

            var eventEntities = new List<TestEventEntity>
            {
                new TestEventEntity(typeof(Monkey), _aggregateId, "test1", 1),
                new TestEventEntity(typeof(Monkey), _aggregateId, "test3", 3),
                new TestEventEntity(typeof(Monkey), _aggregateId, "test2", 2)
            };
            _expectedEvents = eventEntities
                .OrderBy(x => x.Sequence)
                .Select(x => x.Event);

            _systemUnderTest = new TestEventStore();
            _systemUnderTest.Seed(eventEntities);
        };

        Because of = async () =>
        {
            _result = await _systemUnderTest.Get<Monkey>(_aggregateId);
            Console.WriteLine(_result);
        };

        It should_return_the_events_sorted_by_sequence = () =>
        {
            _result.ShouldBeLike(_expectedEvents.OrderBy(x => x));
        };

        Cleanup after = () => { _systemUnderTest.DbContext.Database.EnsureDeleted(); };

        static TestEventStore _systemUnderTest;
        static Guid _aggregateId;
        static IEnumerable<object> _result;
        static IEnumerable<string> _expectedEvents;
    }
}