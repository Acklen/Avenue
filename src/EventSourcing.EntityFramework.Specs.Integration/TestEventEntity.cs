using System;
using System.ComponentModel.DataAnnotations;
using Avenue.EventSourcing.EntityFramework;
using EventSourcing;
using EventSourcing.EntityFramework;

namespace EventSourcing.EntityFramework.Specs.Integration
{
    public class TestEventEntity : IEventEntity
    {
        protected TestEventEntity()
        {
        }

        public TestEventEntity(Type aggregateType, Guid aggregateId, string @event, int sequence)
        {
            AggregateType = aggregateType;
            AggregateId = aggregateId;
            Event = @event;
            Sequence = sequence;
        }

        public virtual Guid Id { get; protected set; }
        public Type AggregateType { get; }

        public virtual Guid AggregateId { get; protected set; }

        public object GetEvent()
        {
            return Event;
        }

        public object GetSequencer()
        {
            return Sequence;
        }

        public virtual string Event { get; protected set; }
        public virtual int Sequence { get; protected set; }
    }
}