using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace EventSourcing.EntityFramework.Specs.Integration
{
    public class TestDbContext : DbContext
    {
        public TestDbContext(DbContextOptions<TestDbContext> options) : base(options)
        {
            Events = new InternalDbSet<TestEventEntity>(this);
        }

        public DbSet<TestEventEntity> Events { get; }
    }
}