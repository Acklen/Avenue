using System.Collections.Generic;
using Avenue.Events;
using Avenue.EventSourcing;
using EventSourcing;

namespace EventSourcing.EntityFramework.Specs.Integration
{
    public class Monkey : AggregateRoot
    {
        public Monkey(IEnumerable<string> domainEvents) : base(domainEvents)
        {
        }
    }
}