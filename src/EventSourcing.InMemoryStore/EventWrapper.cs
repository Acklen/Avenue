﻿using System;

namespace Avenue.EventSourcing.InMemoryStore
{
    public class EventWrapper
    {
        public EventWrapper(Type aggregateRootType, Guid aggregateId, object @event, DateTime time)
        {
            AggregateRootType = aggregateRootType;
            AggregateRootId = aggregateId;
            Event = @event;
            Time = time;
        }

        public Type AggregateRootType { get; private set; }
        public Guid AggregateRootId { get; private set; }
        public object Event { get; private set; }
        public DateTime Time { get; private set; }
    }
}