﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avenue.EventSourcing.InMemoryStore
{
    public class InMemoryEventStore<TDomainEvent> : IEventStore<TDomainEvent>
    {
        static readonly List<EventWrapper> Items = new List<EventWrapper>();
        public List<EventWrapper> Events => Items;

        public Task<IEnumerable<TDomainEvent>> Get<TAggregateRoot>(Guid aggregateId)
        {
            var queueItems = Items.Where(x =>
                Equals(x.AggregateRootId, aggregateId) && x.AggregateRootType == typeof(TAggregateRoot));
            var domainEvents = queueItems.OrderBy(x => x.Time).Select(x => x.Event)
                .Cast<TDomainEvent>();
            return Task.FromResult(domainEvents);
        }

        public Task Write<TAggregateRoot>(Guid aggregateId, IEnumerable<TDomainEvent> @event)
        {
            var events = @event.Select(x =>
                new EventWrapper(typeof(TAggregateRoot), aggregateId, x, DateTime.Now));
            Items.AddRange(events);
            return Task.CompletedTask;
        }

        public void ClearAll()
        {
            Items.Clear();
        }
    }
}