using System;
using Machine.Specifications;

namespace TypeScanner.Specs
{
    public class when_scanning_all_available_assemblies_for_a_single_type
    {
        private static TypeScanning.TypeScanner _systemUnderTest;
        private static Type _result;
        private const string ResultFullName = "TypeScanner.Specs.AfricanPanda";

        Establish _context = () =>
        {
            _systemUnderTest = new TypeScanning.TypeScanner();
        };

        Because of = () => { _result = _systemUnderTest.GetOneType(ResultFullName); };

        It should_return_that_specific_type = () => { _result.FullName.ShouldEqual(ResultFullName); };
    }
}