using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Avenue.Domain;
using Avenue.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Avenue.Persistence.Relational
{
    public class ReadOnlyEntityRepository<TViewModel> : IReadOnlyRepository<TViewModel> where TViewModel : class, IRemovable
    {
        readonly IDbContext _context;

        public ReadOnlyEntityRepository(IDbContext context)
        {
            _context = context;
        }

        public IQueryable<TViewModel> Set()
        {
            return _context.Set<TViewModel>().Where(x=> !x.Removed);
        }

        public async Task<TViewModel> GetById(object id)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));
            
            var entity = await _context.Set<TViewModel>().FindAsync(id);
            if (entity == null) throw new EntityNotFoundException<TViewModel>(id);
            return entity;
        }

        public IQueryable<TViewModel> FindByCondition(IDictionary<string, string> parameters)
        {
            var wheres = parameters.Select(BuildWhere);
            var set = _context.Set<TViewModel>().AsQueryable();
            foreach (var @where in wheres)
            {
                set = set.Where(@where);
            }
            return set;
        }

        static Expression<Func<TViewModel, bool>> BuildWhere(KeyValuePair<string, string> parm)
        {
            var item = Expression.Parameter(typeof(TViewModel), "item");
            var prop = Expression.Property(item, parm.Key);
            var value = Expression.Constant(parm.Value);
            var equal = Expression.Equal(prop, value);
            var lambda = Expression.Lambda<Func<TViewModel, bool>>(equal, item);
            return lambda;
        }
    }
}