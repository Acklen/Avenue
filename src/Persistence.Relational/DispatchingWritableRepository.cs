using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Avenue.Dispatch;
using Avenue.Domain;

namespace Avenue.Persistence.Relational
{
    public class DispatchingWritableRepository<TEntityType> : IWritableRepository<TEntityType> where TEntityType : class
    {
        readonly IWritableRepository<TEntityType> _decoratedRepo;
        readonly IDispatcher<IDomainEvent> _dispatcher;

        public DispatchingWritableRepository(IWritableRepository<TEntityType> decoratedRepo, IDispatcher<IDomainEvent> dispatcher)
        {
            _decoratedRepo = decoratedRepo;
            _dispatcher = dispatcher;
        }

        public Task<TEntityType> Find(Guid id)
        {
            return _decoratedRepo.Find(id);
        }

        public async Task<TEntityType> Create(TEntityType entity)
        {
            var changes = GetChanges(entity); 
            var entityType = await _decoratedRepo.Create(entity);
            await DispatchChanges(changes);
            return entityType;
        }

        public async Task<TEntityType> Update(TEntityType entity)
        {
            var changes = GetChanges(entity); 
            var entityType = await _decoratedRepo.Update(entity);
            await DispatchChanges(changes);
            return entityType;
        }

        IEnumerable<IDomainEvent> GetChanges(TEntityType entity)
        {
            if (entity is IHaveDomainEvents entityWithEvents)
            {
                return entityWithEvents.GetChanges();
            }
            
            return new List<IDomainEvent>();
        }

        async Task DispatchChanges(IEnumerable<IDomainEvent> domainEvents)
        {
            foreach (var domainEvent in domainEvents)
            {
                await _dispatcher.Dispatch(domainEvent);
            }
        }
    }
}