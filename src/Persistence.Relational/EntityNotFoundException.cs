using System;

namespace Avenue.Persistence.Relational
{
    public class EntityNotFoundException<T> : Exception
    {
        public EntityNotFoundException(object id):base($"The {typeof(T)} was not found using id '{id}'.")
        {
            
        }
    }
}