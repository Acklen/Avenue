using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Avenue.Domain;
using Avenue.Persistence.EntityFramework;

namespace Avenue.Persistence.Relational
{
    public class WritableEntityRepository<TEntityType> : IWritableRepository<TEntityType> where TEntityType : class
    {
        readonly IDbContext _dbContext;

        public WritableEntityRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<TEntityType> Find(Guid id)
        {
            var entity = await _dbContext.Set<TEntityType>().FindAsync(id);
            if (entity == null)
            {
                throw new EntityNotFoundException<TEntityType>(id);
            }
            return entity;
        }

        public async Task<TEntityType> Create(TEntityType entity)
        {
            _dbContext.Set<TEntityType>().Add(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<TEntityType> Update(TEntityType entity)
        {
            _dbContext.Set<TEntityType>().Attach(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }
    }
}