using System;
using System.Collections.Generic;
using Avenue.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Pluralize.NET.Core;

namespace Avenue.Persistence.Relational
{
    public class PluralizedModelBuildingStrategy : IModelBuildingStrategy
    {
        readonly IEnumerable<Type> _entityTypes;

        public PluralizedModelBuildingStrategy(params Type[] entityTypes)
        {
            _entityTypes = entityTypes;
        }

        public void Invoke(ModelBuilder modelBuilder)
        {
            foreach (var entity in _entityTypes)
            {
                var entityTypeBuilder = modelBuilder.Entity(entity);
                var entityName = entity.Name;
                var pluralize = new Pluralizer().Pluralize(entityName);
                entityTypeBuilder.ToTable(pluralize);
            }
        }
    }
}