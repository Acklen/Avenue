﻿using System;
using System.Collections.Generic;
using System.Linq;
using Avenue.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Avenue.Persistence.Relational
{
    public class CodeFirstDataContext : DbContext, IDbContext
    {
        readonly IEnumerable<IModelBuildingStrategy> _modelBuildingStrategies;

        public CodeFirstDataContext(DbContextOptions options,
            IEnumerable<IModelBuildingStrategy> modelBuildingStrategies) : base(options)
        {
            _modelBuildingStrategies = modelBuildingStrategies.ToList();
            if (!_modelBuildingStrategies.Any())
            {
                throw new ArgumentException("Must define at least one modelBuildingStrategy.");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            foreach (var modelBuildingStrategy in _modelBuildingStrategies) modelBuildingStrategy.Invoke(modelBuilder);
        }

        public Guid TransactionId => Database.CurrentTransaction.TransactionId;
    }
}