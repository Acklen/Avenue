using System.Threading.Tasks;

namespace Avenue.Commands.Specs
{
    public class TestCommandHandler : ICommandHandler<string>
    {
        public Task Handle(string command)
        {
            return Task.CompletedTask;
        }
    }
}