﻿using System;
using System.Collections.Generic;
using Avenue.Dispatch;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using Stubs;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_command_with_an_exception
    {
        static readonly TestCommand TestCommand = new TestCommand();
        static Exception _exception;
        static Exception _exceptionToThrow;

        Establish context =
            () =>
            {
                _exceptionToThrow = new Exception("test");
                var testCommandHandlerWithException = new TestHandlerWithException().SetException(_exceptionToThrow);
                
                var commandHandlers = new List<ICommandHandler>
                {
                    testCommandHandlerWithException
                };

                _parentDispatcher = Mock.Of<IDispatcher<ICommand>>();
                _dispatcher = new CommandHandlingDispatcherDecorator(_parentDispatcher, commandHandlers);
            };

        Because of =
            () => _exception = Catch.Exception(() => _dispatcher.Dispatch(TestCommand).Await());

        It should_throw_a_not_implemented_exception =
            () => _exception.InnerException.Should().Be(_exceptionToThrow);

        static IDispatcher<ICommand> _dispatcher;
        static IDispatcher<ICommand> _parentDispatcher;
    }
}