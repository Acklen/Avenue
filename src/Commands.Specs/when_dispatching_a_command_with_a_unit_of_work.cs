using System;
using System.Threading.Tasks;
using Avenue.Commands.EntityFramework;
using Avenue.Dispatch;
using Avenue.Domain;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_command_with_a_unit_of_work
    {
        Establish _context = () =>
        {
            _unitOfWork = Mock.Of<IUnitOfWork>();
            _dispatcher = Mock.Of<IDispatcher<ICommand>>();
            _systemUnderTest = new UnitOfWorkCommandDispatcherDecorator(_dispatcher, _unitOfWork);

            _command = Mock.Of<ICommand>();

            Mock.Get(_unitOfWork)
                .Setup(x => x.Execute(Moq.It.IsAny<Func<Task>>()))
                .Callback<Func<Task>>(async x => await x());
        };

        Because of = () => { _systemUnderTest.Dispatch(_command).Wait(); };

        It should_dispatch = () => { Mock.Get(_dispatcher).Verify(x => x.Dispatch(_command)); };

        static IUnitOfWork _unitOfWork;
        static IDispatcher<ICommand> _dispatcher;
        static UnitOfWorkCommandDispatcherDecorator _systemUnderTest;
        static ICommand _command;
    }
}