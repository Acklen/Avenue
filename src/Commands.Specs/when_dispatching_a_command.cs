﻿using System.Collections.Generic;
using Avenue.Dispatch;
using Machine.Specifications;
using Moq;
using Stubs;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_command
    {
        static CommandHandlingDispatcherDecorator _handlingDispatcher;
        static IDispatcher<ICommand> _parentDispatcher;
        static TestHandler _testHandler;
        static readonly TestCommand TestCommand = new TestCommand();

        Establish context =
            () =>
            {
                _testHandler = new TestHandler();
                var commandHandlers = new List<ICommandHandler>
                {
                    _testHandler,
                    Mock.Of<ICommandHandler>()
                };

                _parentDispatcher = Mock.Of<IDispatcher<ICommand>>();
                _handlingDispatcher = new CommandHandlingDispatcherDecorator(_parentDispatcher, commandHandlers);
            };

        Because of =
            () => _handlingDispatcher.Dispatch(TestCommand).Await();

        It should_dispatch_the_expected_command =
            () => _testHandler.CommandHandled.ShouldEqual(TestCommand);

        It should_dispatch_the_decorated_dispatcher =
            () => Mock.Get(_parentDispatcher).Verify(x => x.Dispatch(TestCommand));
    }
}