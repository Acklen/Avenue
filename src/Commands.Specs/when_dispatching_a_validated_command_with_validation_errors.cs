using System;
using System.Collections.Generic;
using Avenue.Dispatch;
using Machine.Specifications;
using Moq;
using Stubs;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_validated_command_with_validation_errors
    {
        static ValidatingCommandDispatcher _commandDispatcher;
        static readonly TestCommand TestCommand = new TestCommand();
        static Exception _exception;

        Establish context =
            () =>
            {
                var commandValidators = new List<ICommandValidator>()
                {
                    new ValidatorWithError()
                };

                _commandDispatcher = new ValidatingCommandDispatcher(Mock.Of<IDispatcher<ICommand>>(), commandValidators);
            };

        Because of =
            () => _exception = Catch.Exception(() => _commandDispatcher.Dispatch(TestCommand).Await());

        It should_throw_a_not_implemented_exception =
            () => _exception.Message.ShouldEqual("Validation error");
    }
}