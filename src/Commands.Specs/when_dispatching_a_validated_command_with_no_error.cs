using System.Collections.Generic;
using Avenue.Dispatch;
using Machine.Specifications;
using Moq;
using Stubs;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_validated_command_with_no_error
    {
        static ValidatingCommandDispatcher _commandDispatcher;
        static readonly TestCommand TestCommand = new TestCommand();
        static Stubs.TestCommandValidator _testCommandValidator;

        Establish context =
            () =>
            {
                _testCommandValidator = new Stubs.TestCommandValidator();
                var commandValidators = new List<ICommandValidator>
                {
                    _testCommandValidator
                };

                _decoratedDispatcher = Mock.Of<IDispatcher<ICommand>>();
                _commandDispatcher = new ValidatingCommandDispatcher(_decoratedDispatcher, commandValidators);
            };

        Because of =
            () => _commandDispatcher.Dispatch(TestCommand).Await();

        It should_validate_the_command =
            () => _testCommandValidator.CommandValidated.ShouldEqual(TestCommand);

        It should_dispatch_the_decorated_dispatcher =
            () => Mock.Get(_decoratedDispatcher).Verify(x => x.Dispatch(TestCommand));

        static IDispatcher<ICommand> _decoratedDispatcher;
    }
}