using Avenue.Dispatch;
using Avenue.Domain;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_queued_command
    {
        Establish _context = () =>
        {
            _publisher = Mock.Of<IPublisher<ICommand>>();
            _decoratedDispatcher = Mock.Of<IDispatcher<ICommand>>();
            _systemUnderTest = new QueuedCommandDispatcherDecorator(_decoratedDispatcher, _publisher);
            
            _command = Mock.Of<ICommand>();
        };

        Because of = () =>
        {
            _systemUnderTest.Dispatch(_command).Wait();
        };

        It should_publish_the_command = () => { Mock.Get(_publisher).Verify(x => x.Publish(_command)); };

        It should_dispatch_to_the_parent_dispatcher = () =>
        {
            Mock.Get(_decoratedDispatcher).Verify(x => x.Dispatch(_command));
        };
        
        static IPublisher<ICommand> _publisher;
        static IDispatcher<ICommand> _decoratedDispatcher;
        static QueuedCommandDispatcherDecorator _systemUnderTest;
        static ICommand _command;
    }
}