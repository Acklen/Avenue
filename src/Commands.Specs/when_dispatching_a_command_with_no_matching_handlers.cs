﻿using System;
using System.Collections.Generic;
using Avenue.Dispatch;
using Machine.Specifications;
using Moq;
using Stubs;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_command_with_no_matching_handlers
    {
        static readonly TestCommand TestCommand = new TestCommand();
        static Exception _exception;

        Establish context =
            () =>
            {
                var commandHandlers = new List<ICommandHandler>
                {
                    Mock.Of<ICommandHandler>()
                };

                _commandDispatcher = Mock.Of<IDispatcher<ICommand>>();
                _dispatcher = new CommandHandlingDispatcherDecorator(_commandDispatcher, commandHandlers);
            };

        Because of =
            () => _exception = Catch.Exception(() => _dispatcher.Dispatch(TestCommand).Await());

        It should_throw_a_not_implemented_exception =
            () => _exception.ShouldBeOfExactType<NotImplementedException>();

        It should_dispatch_the_decorated_dispatcher =
            () => Mock.Get(_commandDispatcher).Verify(x => x.Dispatch(TestCommand));

        static IDispatcher<ICommand> _dispatcher;
        static IDispatcher<ICommand> _commandDispatcher;
    }
}