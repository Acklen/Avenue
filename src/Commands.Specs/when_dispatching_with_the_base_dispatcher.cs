using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_with_the_base_dispatcher
    {
        Establish _context = () =>
        {
            _systemUnderTest = new BaseCommandDispatcher();
            _commandOrEvent = Mock.Of<ICommand>();
        };

        Because of = () =>
        {
            _systemUnderTest.Dispatch(_commandOrEvent).Wait();
        };

        It should_do_nothing = () => { Mock.Get(_commandOrEvent).VerifyNoOtherCalls(); };
        
        static BaseCommandDispatcher _systemUnderTest;
        static ICommand _commandOrEvent;
    }
}