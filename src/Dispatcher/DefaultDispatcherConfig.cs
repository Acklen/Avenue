namespace Avenue.Dispatch
{
    public class DefaultDispatcherConfig : IDispatcherConfig
    {
        public DefaultDispatcherConfig()
        {
            MaximumHandlersPerDispatch = 1;
            MinimumHandlersPerDispatch = 1;
        }

        public int MaximumHandlersPerDispatch { get; }
        public int MinimumHandlersPerDispatch { get; }
    }
}