using System.Threading.Tasks;
using Avenue.Domain;

namespace Avenue.Dispatch
{
    public class SubscribingDispatcherDecorator<T> : IDispatcher<T>
    {
        public SubscribingDispatcherDecorator(IDispatcher<T> parentDispatcher, ISubscriber<T> subscriber)
        {
            subscriber.Subscribe(async item =>
            {
                
                await parentDispatcher.Dispatch(item);
            });
        }

        public Task Dispatch(T commandOrEvent)
        {
            return Task.CompletedTask;
        }
    }
}