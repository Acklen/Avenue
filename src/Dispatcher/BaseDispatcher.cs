using System.Threading.Tasks;

namespace Avenue.Dispatch
{
    public class BaseDispatcher<T> : IDispatcher<T>
    {
        public Task Dispatch(T commandOrEvent)
        {
            return Task.CompletedTask;
        }
    }
}