using System.Threading.Tasks;
using Avenue.Domain;

namespace Avenue.Dispatch
{
    public class LoggedDispatcherDecorator<T> : IDispatcher<T>
    {
        readonly IDispatcher<T> _parentDispatcher;
        readonly IDispatcherLogger _logger;
        readonly ITimeProvider _timeProvider;

        public LoggedDispatcherDecorator(IDispatcher<T> parentDispatcher, IDispatcherLogger logger,
            ITimeProvider timeProvider)
        {
            _parentDispatcher = parentDispatcher;
            _logger = logger;
            _timeProvider = timeProvider;
        }
        public async Task Dispatch(T commandOrEvent)
        {
            try
            {
                await _logger.LogInfo(this, _timeProvider.GetNow(), commandOrEvent, LogTiming.Before);
                await _parentDispatcher.Dispatch(commandOrEvent);
                await _logger.LogInfo(this, _timeProvider.GetNow(), commandOrEvent, LogTiming.After);
            }
            catch (HandlerInvocationException ex)
            {
                await _logger.LogException(ex.Handler, _timeProvider.GetNow(), ex, commandOrEvent);
                throw ex.InnerException;
            }
        }
    }
}