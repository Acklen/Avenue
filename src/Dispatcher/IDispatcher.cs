using System.Threading.Tasks;

namespace Avenue.Dispatch
{
    public interface IDispatcher
    {
    }

    public interface IDispatcher<in T> : IDispatcher
    {
        Task Dispatch(T commandOrEvent);
    }
}