using System;
using System.Threading.Tasks;

namespace Avenue.Dispatch
{
    public class QuietDispatcherLogger : IDispatcherLogger
    {
        public Task LogInfo(object sender, DateTime timeStamp, object command, LogTiming logTiming = LogTiming.NotSpecified)
        {
            Console.WriteLine($"INFO: {logTiming} {sender.GetType()} dispatched {command.GetType()} at {timeStamp}");
            return Task.CompletedTask;
        }

        public Task LogException(object sender, DateTime timeStamp, Exception exception, object command)
        {
            Console.WriteLine($"ERROR: {sender.GetType()} failed to dispatch {command.GetType()} at {timeStamp}");
            return Task.CompletedTask;
        }
    }
}