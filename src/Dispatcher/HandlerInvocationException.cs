using System;

namespace Avenue.Dispatch
{
    public class HandlerInvocationException : Exception
    {
        public object Handler { get; }
        public object[] MethodArgs { get; }

        public HandlerInvocationException(object handler, object[] methodArgs, Exception innerException) :
            base(
                $"There was an error when attempting to handle a command with '{handler.GetType()}'. See inner exception for details.",
                innerException)
        {
            Handler = handler;
            MethodArgs = methodArgs;
        }
    }
}