using System;

namespace Avenue.Dispatch
{
    public class TooManyHandlersException : Exception
    {
        public TooManyHandlersException(object command, int actualHandlerCount, int maxHandlers) : base(
            $"Too many handlers were found that match the command {nameof(command)}. The max is {maxHandlers} but found {actualHandlerCount}.")
        {
        }
    }
}