namespace Avenue.Dispatch
{
    public enum LogTiming
    {
        NotSpecified,
        Before,
        After
    }
}