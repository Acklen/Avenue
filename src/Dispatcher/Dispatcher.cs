using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Avenue.Dispatch
{
    public class Dispatcher<T> : IDispatcher<T>
    {
        const string HandleMethodName = "Handle";
        readonly IDispatcherConfig _config;
        readonly IHandlerMatcher _handlerMatcher;
        
        public Dispatcher(IHandlerMatcher handlerMatcher, IDispatcherConfig config = null)
        {
            _handlerMatcher = handlerMatcher;
            _config = config ?? new DefaultDispatcherConfig();
        }

        public async Task Dispatch(T commandOrEvent)
        {
            if (commandOrEvent == null) throw new ArgumentNullException(nameof(commandOrEvent));
            var commandHandlers = GetListOfHandlerThatMatchCommand(commandOrEvent);
            VerifyHandlersSatisfyConfiguration(commandOrEvent, commandHandlers);
            foreach (var handler in commandHandlers) await Handle(handler, commandOrEvent).ConfigureAwait(false);
        }

        IList<object> GetListOfHandlerThatMatchCommand(object commandOrEvent)
        {
            var matchingCommandHandlers = _handlerMatcher.GetMatchingHandlers(commandOrEvent);
            var commandHandlers = matchingCommandHandlers as IList<object> ??
                                  matchingCommandHandlers.Cast<object>().ToList();
            return commandHandlers;
        }

        void VerifyHandlersSatisfyConfiguration(T commandOrEvent, IList<object> commandHandlers)
        {
            var handlerCount = commandHandlers.Count();
            if (handlerCount < _config.MinimumHandlersPerDispatch)
                throw new NotImplementedException(
                    $"No handlers could be found to handle the '{commandOrEvent.GetType().Name}' command type .");

            if (handlerCount > _config.MaximumHandlersPerDispatch)
                throw new TooManyHandlersException(commandOrEvent, handlerCount,
                    _config.MaximumHandlersPerDispatch);
        }

        async Task Handle(object invokableObject, params object[] methodArgs)
        {
            var methodArgTypes = methodArgs.Select(x => x.GetType()).ToArray();
            try
            {
                var handlerMethod = invokableObject.GetType().GetMethod(HandleMethodName, methodArgTypes);
                await ((Task) handlerMethod.Invoke(invokableObject, methodArgs)).ConfigureAwait(false);
            }
            catch (TargetInvocationException ex)
            {
                throw new HandlerInvocationException(invokableObject, methodArgs, ex.InnerException);
            }
            catch (AggregateException ex)
            {
                throw new HandlerInvocationException(invokableObject, methodArgs, ex.InnerException);
            }
            catch (Exception ex)
            {
                throw new HandlerInvocationException(invokableObject, methodArgs, ex.GetBaseException());
            }
        }
    }
}