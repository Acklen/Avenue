using System;
using System.Threading.Tasks;

namespace Avenue.Dispatch
{
    public interface IDispatcherLogger
    {
        Task LogInfo(object sender, DateTime timeStamp, object command, LogTiming logTiming = LogTiming.NotSpecified);
        Task LogException(object sender, DateTime timeStamp, Exception exception, object command);
    }
}