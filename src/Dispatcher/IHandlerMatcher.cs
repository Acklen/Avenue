using System.Collections;

namespace Avenue.Dispatch
{
    public interface IHandlerMatcher
    {
        IEnumerable GetMatchingHandlers(object command);
    }
}