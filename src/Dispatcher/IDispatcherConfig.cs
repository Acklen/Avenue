namespace Avenue.Dispatch
{
    public interface IDispatcherConfig
    {
        int MaximumHandlersPerDispatch { get; }
        int MinimumHandlersPerDispatch { get; }
    }
}