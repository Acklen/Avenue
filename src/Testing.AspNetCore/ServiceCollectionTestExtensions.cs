using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.Testing.AspNetCore
{
    public static class ServiceCollectionTestExtensions
    {
        public static ResolutionStatus ShouldBeAbleToResolve<T>(this IServiceCollection services)
        {
            var serviceDescriptors = services.Where(x => x.ServiceType == typeof(T)).ToArray();
            if (!serviceDescriptors.Any())
                throw new Exception(
                    $"The service collection does not have any service types that match '{nameof(T)}.'");
            return new ResolutionStatus(serviceDescriptors);
        }

        public static T ShouldResolve<T>(this IServiceCollection services)
        {
            var service = services.GetService<T>();
            if (service == null) throw new Exception($"Could not resolve type {typeof(T).Name}.");
            return service;
        }

        public static T GetService<T>(this IServiceCollection services)
        {
            var buildServiceProvider = services.BuildServiceProvider();
            var service = buildServiceProvider.GetRequiredService<T>();
            return service;
        }
    }
}