using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.Testing.AspNetCore
{
    public class ResolutionStatus
    {
        readonly IEnumerable<ServiceDescriptor> _serviceDescriptors;

        internal ResolutionStatus(IEnumerable<ServiceDescriptor> serviceDescriptors)
        {
            _serviceDescriptors = serviceDescriptors;
        }

        public ImplementationServiceDescriptor WithImplementation<T>()
        {
            var matchingServiceDescriptor = _serviceDescriptors.FirstOrDefault(x =>
                x.ImplementationType == typeof(T) || x.ImplementationInstance != null &&
                x.ImplementationInstance.GetType() == typeof(T));
            if (matchingServiceDescriptor == null)
            {
                var serviceTypeNames = string.Join(", ", _serviceDescriptors.Select(x => x.ServiceType.Name).ToArray());

                throw new Exception(
                    $"The service descriptor is configured to resolve to type(s) {serviceTypeNames}. The given type {typeof(T).Name} was not listed as an implementation.");
            }

            return new ImplementationServiceDescriptor(matchingServiceDescriptor);
        }
    }

    public class ImplementationServiceDescriptor
    {
        readonly ServiceDescriptor _serviceDescriptor;

        public ImplementationServiceDescriptor(ServiceDescriptor serviceDescriptor)
        {
            _serviceDescriptor = serviceDescriptor;
        }

        public ImplementationServiceDescriptor AsA(ServiceLifetime lifetime)
        {
            var isMatch = _serviceDescriptor.Lifetime == lifetime;
            if (!isMatch)
                throw new Exception(
                    $"Expected {_serviceDescriptor.ServiceType.Name} to be lifetime {lifetime.ToString()} but was scoped as {_serviceDescriptor.Lifetime}.");

            return this;
        }

        public ImplementationServiceDescriptor AsASingleton()
        {
            return AsA(ServiceLifetime.Singleton);
        }
    }
}