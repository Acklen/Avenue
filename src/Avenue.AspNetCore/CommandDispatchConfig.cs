using System;
using System.Collections.Generic;
using System.Reflection;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Domain;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.AspNetCore
{
    public class CommandDispatchConfig
    {
        readonly IPublisher<ICommand> _publisher;
        IEnumerable<Assembly> _commandValidatorAssemblies;
        readonly Type _baseDispatcherType;
        readonly List<Type> _dispatchDecorators;
        Type _loggerType;

        public CommandDispatchConfig(IPublisher<ICommand> publisher)
        {
            _publisher = publisher;
            _commandValidatorAssemblies = new List<Assembly>();
            _baseDispatcherType = typeof(BaseCommandDispatcher);
            _dispatchDecorators = new List<Type> {typeof(QueuedCommandDispatcherDecorator)};
        }
        
        public IServiceCollection Execute(IServiceCollection services)
        {
            var dispatcherServiceType = typeof(IDispatcher<ICommand>);
            
            services
                .AddTransient(dispatcherServiceType, _baseDispatcherType)
                .AddSingleton(_publisher)
                .Scan(t => t
                    .FromAssemblies(_commandValidatorAssemblies)
                    .AddClasses(c => c.AssignableTo(typeof(ICommandValidator)))
                    .AsSelfWithInterfaces()
                    .WithTransientLifetime());

            _dispatchDecorators.ForEach(decoratorType => services.Decorate(dispatcherServiceType, decoratorType));

            if (_loggerType != null)
                services.AddTransient(typeof(IDispatcherLogger), _loggerType);
            
            return services;
        }

        public CommandDispatchConfig WithCommandValidatorsInAssemblies(params Assembly[] assembly)
        {
            _commandValidatorAssemblies = assembly;
            _dispatchDecorators.Add(typeof(ValidatingCommandDispatcher));
            return this;
        }

        public CommandDispatchConfig WithLogging<T>()
        {
            _dispatchDecorators.Add(typeof(LoggedDispatcherDecorator<ICommand>));
            _loggerType = typeof(T);
            return this;
        }
    }
}