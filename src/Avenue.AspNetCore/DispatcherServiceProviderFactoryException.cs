using System;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.AspNetCore
{
    public class DispatcherServiceProviderFactoryException : Exception
    {
        public IServiceCollection Services { get; }

        public DispatcherServiceProviderFactoryException(IServiceCollection services, Exception innerException)
            :base("An exception occurred when trying to build the dispatcher service provider. See inner exception for details.")
        {
            Services = services;
        }
    }
}