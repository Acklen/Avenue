using System.Linq;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Avenue.AspNetCore
{
    public class RoutePrefixConvention : IApplicationModelConvention
    {
        readonly AttributeRouteModel _routePrefix;

        public RoutePrefixConvention(IRouteTemplateProvider route)
        {
            _routePrefix = new AttributeRouteModel(route);
        }

        public void Apply(ApplicationModel application)
        {
            foreach (var selector in application.Controllers.SelectMany(c => c.Selectors))
                if (selector.AttributeRouteModel != null)
                    selector.AttributeRouteModel =
                        AttributeRouteModel.CombineAttributeRouteModel(_routePrefix, selector.AttributeRouteModel);
                else
                    selector.AttributeRouteModel = _routePrefix;
        }
    }
}