using System;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.AspNetCore
{
    public static class DecorationExtensions
    {

        public static IServiceCollection AttemptDecorate<TService, TImpl>(this IServiceCollection services)
        {
            return AttemptDecorate(services, typeof(TService), typeof(TImpl));
        }
        
        public static IServiceCollection AttemptDecorate(this IServiceCollection services, Type service, Type impl)
        {
            try
            {
                return services.Decorate(service, impl);
            }
            catch (Exception ex)
            {
                throw new Exception($"Could not decorate {service} with {impl}.", ex);
            }
            
        }
    }
}