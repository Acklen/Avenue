using System;
using Avenue.Commands;
using Avenue.Domain;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.AspNetCore
{
    public static class LocalCommandDispatchExtensions
    {
        public static IServiceCollection AddLocalCommandDispatcher(this IServiceCollection services, IPublisher<ICommand> commandQueue, Action<CommandDispatchConfig> configAction = null)
        {
            var commandDispatchConfig = new CommandDispatchConfig(commandQueue);
            configAction?.Invoke(commandDispatchConfig);

            return commandDispatchConfig.Execute(services);
        }
    }
}