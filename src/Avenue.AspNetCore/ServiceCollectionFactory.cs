using Microsoft.Extensions.DependencyInjection;

namespace Avenue.AspNetCore
{
    public class ServiceCollectionFactory : IServiceCollectionFactory
    {
        public IServiceCollection Create()
        {
            return new ServiceCollection();
        }
    }
}