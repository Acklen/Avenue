using Microsoft.Extensions.DependencyInjection;

namespace Avenue.AspNetCore
{
    public interface IServiceCollectionFactory
    {
        IServiceCollection Create();
    }
}