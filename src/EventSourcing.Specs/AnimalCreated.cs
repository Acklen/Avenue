﻿using System;
using Avenue.Domain;
using Avenue.Events;

namespace EventSourcing.Specs
{
    public class AnimalCreated : IDomainEvent
    {
        public Guid Id { get; private set; }

        public AnimalCreated(Guid id)
        {
            Id = id;
        }
    }
}