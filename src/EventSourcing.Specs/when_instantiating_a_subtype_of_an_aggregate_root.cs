﻿using System;
using System.Collections.Generic;
using Avenue.Domain;
using Avenue.Events;
using Machine.Specifications;

namespace EventSourcing.Specs
{
    public class when_instantiating_a_subtype_of_an_aggregate_root
    {
        static IEnumerable<IDomainEvent> _events;
        static Squirrel _animal;
        static Guid _id;

        Establish context =
            () =>
            {
                _id = Guid.NewGuid();

                _events = new List<IDomainEvent>
                {
                    new SquirrelCreated(_id),
                    new TreeClimbed()
                };
            };

        Because of =
            () => _animal = new Squirrel(_events);

        It should_have_climbed = () => _animal.Position.ShouldEqual("tree");

        It should_have_the_same_id = () => _animal.Id.ShouldEqual(_id);
    }
}