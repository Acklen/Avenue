﻿using System;
using System.Collections.Generic;
using Avenue.Domain;
using Avenue.Events;

namespace EventSourcing.Specs
{
    public class Squirrel : Animal
    {
        public string Position { get; private set; }

        public Squirrel(Guid id) : base(new List<IDomainEvent>())
        {
            When(NewChange(new SquirrelCreated(id)));
        }

        void When(SquirrelCreated newEvent)
        {
            Id = newEvent.Id;
        }

        public Squirrel(IEnumerable<IDomainEvent> domainEvents) : base(domainEvents)
        {
        }

        public void ClimbTree()
        {
            When(NewChange(new TreeClimbed()));
        }

        void When(TreeClimbed newEvent)
        {
            Position = "tree";
        }
    }
}