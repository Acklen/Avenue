﻿using System;
using System.Collections.Generic;
using Avenue.Domain;
using Avenue.Events;
using Machine.Specifications;

namespace EventSourcing.Specs
{
    public class when_instantiating_an_aggregate_root
    {
        static IEnumerable<IDomainEvent> _events;
        static Animal _animal;
        static Guid _id;

        Establish context =
            () =>
            {
                _id = Guid.NewGuid();

                _events = new List<IDomainEvent>
                {
                    new AnimalCreated(_id),
                    new AnimalAte("nuts")
                };
            };

        Because of =
            () => _animal = new Animal(_events);

        It should_have_eaten =
            () => _animal.Meals.ShouldContain("nuts");

        It should_have_the_same_id = () => _animal.Id.ShouldEqual(_id);
    }
}