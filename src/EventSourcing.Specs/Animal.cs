﻿using System;
using System.Collections.Generic;
using Avenue.Domain;
using Avenue.Events;
using Avenue.EventSourcing;
using EventSourcing;

namespace EventSourcing.Specs
{
    public class Animal : AggregateRoot
    {
        public Animal(Guid id) : base(new List<object>())
        {
            When(NewChange(new AnimalCreated(id)));
        }

        void When(AnimalCreated newEvent)
        {
            Id = newEvent.Id;
        }

        public Animal(IEnumerable<IDomainEvent> domainEvents) : base(domainEvents)
        {
        }

        public List<string> Meals { get; } = new List<string>();

        public void Eat(string food)
        {
            When(NewChange(new AnimalAte(food)));
        }

        void When(AnimalAte newEvent)
        {
            Meals.Add(newEvent.Food);
        }
    }
}