using System;
using System.Collections.Generic;
using Avenue.Domain;
using Avenue.Events;
using Avenue.EventSourcing;
using EventSourcing;
using Machine.Specifications;

namespace EventSourcing.Specs
{
    public class when_instantiating_an_aggregate_with_unsupported_domain_events
    {
        static IEnumerable<IDomainEvent> _events;
        static Guid _id;
        static Exception _exception;

        Establish context =
            () =>
            {
                _id = Guid.NewGuid();

                _events = new List<IDomainEvent>
                {
                    new SquirrelCreated(_id)
                };
            };

        Because of =
            () => _exception = Catch.Exception(() => new Animal(_events));

        It should_throw_an_exception =
            () => _exception.ShouldBeOfExactType<CannotMutateAggregateWithUnknownEventException>();
    }
}