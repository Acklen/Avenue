﻿using Avenue.Domain;
using Avenue.Events;

namespace EventSourcing.Specs
{
    public class AnimalAte: IDomainEvent
    {
        public string Food { get; private set; }

        public AnimalAte(string food)
        {
            Food = food;
        }
    }
}