﻿using System;
using Avenue.Domain;
using Avenue.Events;

namespace EventSourcing.Specs
{
    public class SquirrelCreated:IDomainEvent
    
    {
        public Guid Id { get; private set; }

        public SquirrelCreated(Guid id)
        {
            Id = id;
        }
    }
}