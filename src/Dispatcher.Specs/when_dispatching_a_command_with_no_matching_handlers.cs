﻿using System;
using System.Collections.Generic;
using System.Linq;
using Avenue.Dispatch;
using Machine.Specifications;
using Moq;
using Stubs;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_command_with_no_matching_handlers
    {
        static Dispatcher<object> _dispatcher;
        static readonly TestCommand TestCommand = new TestCommand();
        static Exception _exception;

        Establish context =
            () =>
            {
                var commandHandlers = new List<ICommandHandler>
                {
                    Mock.Of<ICommandHandler>()
                };
                _dispatcher = new Dispatcher<object>(new DefaultHandlerMatcher(commandHandlers.Cast<IHandler>()));
            };

        Because of =
            () => _exception = Catch.Exception(() => _dispatcher.Dispatch(TestCommand).Await());

        It should_throw_a_not_implemented_exception =
            () => _exception.ShouldBeOfExactType<NotImplementedException>();
    }
}