﻿using System.Threading.Tasks;
using Avenue.Dispatch;

namespace Avenue.Commands.Specs.Stubs
{
    public class TestHandler : ICommandHandler<TestCommand>
    {
        public TestCommand CommandHandled { get; private set; }

        public Task Handle(TestCommand command)
        {
            CommandHandled = command;
            return Task.CompletedTask;
        }
    }
}