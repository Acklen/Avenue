﻿namespace Avenue.Commands.Specs.Stubs
{
    public class TestEvent
    {
        public TestEvent(object command)
        {
            Command = command;
        }

        public object Command { get; }
    }
}