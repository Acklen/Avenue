﻿using System;
using System.Threading.Tasks;
using Avenue.Dispatch;

namespace Avenue.Commands.Specs.Stubs
{
    public class TestHandlerWithException : ICommandHandler<TestCommand>
    {
        readonly Exception _exceptionToThrow;

        public TestHandlerWithException(Exception exceptionToThrow)
        {
            _exceptionToThrow = exceptionToThrow;
        }

        public Task Handle(TestCommand command)
        {
            throw _exceptionToThrow;
        }
    }
}