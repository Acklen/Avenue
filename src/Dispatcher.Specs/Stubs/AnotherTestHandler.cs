﻿using System.Threading.Tasks;
using Avenue.Dispatch;

namespace Avenue.Commands.Specs.Stubs
{
    public class AnotherTestHandler : ICommandHandler<AnotherTestCommand>
    {
        public AnotherTestCommand CommandHandled { get; private set; }

        public Task Handle(AnotherTestCommand command)
        {
            CommandHandled = command;
            return Task.CompletedTask;
        }
    }
}