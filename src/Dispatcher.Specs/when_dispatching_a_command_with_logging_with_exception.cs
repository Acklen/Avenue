using System;
using System.Threading.Tasks;
using Avenue.Dispatch;
using Avenue.Domain;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_command_with_logging_with_exception
    {
        Establish _context = () =>
        {
            _dispatcher = Mock.Of<IDispatcher<ICommand>>();
            _dispatcherLogger = Mock.Of<IDispatcherLogger>();
            _timeProvider = Mock.Of<ITimeProvider>();
            _systemUnderTest = new LoggedDispatcherDecorator<ICommand>(_dispatcher, _dispatcherLogger, _timeProvider);

            _now = DateTime.Now;
            Mock.Get(_timeProvider).Setup(x => x.GetNow()).Returns(_now);
            
            _command = Mock.Of<ICommand>();

            _testCommandHandler = Mock.Of<ICommandHandler>();
            _handlerException = new Exception("handler exception");
            _dispatcherException = new HandlerInvocationException(_testCommandHandler, new object[]{_command}, _handlerException);
            Mock.Get(_dispatcher).Setup(x => x.Dispatch(_command)).ThrowsAsync(_dispatcherException);
        };

        Because of = () => { _exceptionThrownToWorld = Catch.Exception(()=> _systemUnderTest.Dispatch(_command).Wait()); };

        It should_dispatch = () => { Mock.Get(_dispatcher).Verify(x => x.Dispatch(_command)); };

        It should_log= () =>
            Mock.Get(_dispatcherLogger).Verify<Task>(x => x.LogException(_testCommandHandler, _now, _dispatcherException, _command));

        It should_throw_the_exception_to_the_world = () => _exceptionThrownToWorld.InnerException.Should().BeEquivalentTo(_handlerException);
        
        static IDispatcher<ICommand> _dispatcher;
        static LoggedDispatcherDecorator<ICommand> _systemUnderTest;
        static ICommand _command;
        static IDispatcherLogger _dispatcherLogger;
        static ITimeProvider _timeProvider;
        static DateTime _now;
        static Exception _dispatcherException;
        static Exception _exceptionThrownToWorld;
        static Exception _handlerException;
        static ICommandHandler _testCommandHandler;
    }
}