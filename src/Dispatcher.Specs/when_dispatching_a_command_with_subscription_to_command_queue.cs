using System.Threading;
using Avenue.Dispatch;
using Avenue.Domain;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_command_with_subscription_to_command_queue
    {
        Establish _context = () =>
        {
            _dispatcher = Mock.Of<IDispatcher<ICommand>>();
            _subscriber = new PubSub<ICommand>();
            new SubscribingDispatcherDecorator<ICommand>(_dispatcher, _subscriber);

            _command = Mock.Of<ICommand>();
        };

        Because of = () => { _subscriber.Publish(_command).Wait(); };

        It should_dispatch = () =>
        { 
            Thread.Sleep(500);
            Mock.Get(_dispatcher).Verify(x => x.Dispatch(_command));
        };
        
        static IDispatcher<ICommand> _dispatcher;
        static ICommand _command;
        static PubSub<ICommand> _subscriber;
    }
}