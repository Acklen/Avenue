﻿using System;
using System.Collections.Generic;
using System.Linq;
using Avenue.Dispatch;
using FluentAssertions;
using Machine.Specifications;
using Stubs;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_command_where_the_handler_has_an_exception
    {
        static Dispatcher<object> _dispatcher;
        static readonly TestCommand TestCommand = new TestCommand();
        static Exception _exception;
        static Exception _exceptionToThrow;

        Establish context =
            () =>
            {
                _exceptionToThrow = new Exception("Test");
                _testHandlerWithException = new TestHandlerWithException().SetException(_exceptionToThrow);
                var commandHandlers = new List<ICommandHandler>
                {
                    _testHandlerWithException
                };

                _dispatcher = new Dispatcher<object>(new DefaultHandlerMatcher(commandHandlers.Cast<IHandler>()));
            };

        Because of =
            () => _exception = Catch.Exception(() => _dispatcher.Dispatch(TestCommand).Await());

        It should_bubble_up_the_exception =
            () => _exception.InnerException.Should().Be(_exceptionToThrow);

        It should_wrap_the_exception = () => _exception.Should().BeOfType<HandlerInvocationException>();

        static TestHandlerWithException _testHandlerWithException;
    }
}