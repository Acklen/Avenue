﻿using System;
using System.Collections.Generic;
using System.Linq;
using Avenue.Dispatch;
using Machine.Specifications;
using Moq;
using Stubs;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_command
    {
        static Dispatcher<ICommand> _dispatcher;
        static TestHandler _testHandler;
        static readonly TestCommand TestCommand = new TestCommand();

        Establish context =
            () =>
            {
                _testHandler = new TestHandler();
                var commandHandlers = new List<ICommandHandler>
                {
                    _testHandler,
                    Mock.Of<ICommandHandler>()
                };
                _dispatcher = new Dispatcher<ICommand>(new DefaultHandlerMatcher(commandHandlers.Cast<IHandler>()));
            };

        Because of =
            () => _dispatcher.Dispatch(TestCommand).Await();

        It should_dispatch_the_expected_command =
            () => _testHandler.CommandHandled.ShouldEqual(TestCommand);
    }
}