using System;
using Avenue.Dispatch;
using Avenue.Domain;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Avenue.Commands.Specs
{
    public class when_dispatching_a_command_with_logging
    {
        Establish _context = () =>
        {
            _dispatcher = Mock.Of<IDispatcher<ICommand>>();
            _dispatcherLogger = Mock.Of<IDispatcherLogger>();
            _timeProvider = Mock.Of<ITimeProvider>();
            _systemUnderTest = new LoggedDispatcherDecorator<ICommand>(_dispatcher, _dispatcherLogger, _timeProvider);

            _now = DateTime.Now;
            Mock.Get(_timeProvider).Setup(x => x.GetNow()).Returns(_now);
            _command = Mock.Of<ICommand>();
        };

        Because of = () => { _systemUnderTest.Dispatch(_command).Wait(); };

        It should_dispatch = () => { Mock.Get(_dispatcher).Verify(x => x.Dispatch(_command)); };

        It should_log_before = () =>
            Mock.Get(_dispatcherLogger)
                .Verify(x => x.LogInfo(_systemUnderTest, _now, _command, LogTiming.Before));

        It should_log_after= () =>
            Mock.Get(_dispatcherLogger)
                .Verify(x => x.LogInfo(_systemUnderTest, _now, _command, LogTiming.After));
        
        static IDispatcher<ICommand> _dispatcher;
        static LoggedDispatcherDecorator<ICommand> _systemUnderTest;
        static ICommand _command;
        static IDispatcherLogger _dispatcherLogger;
        static ITimeProvider _timeProvider;
        static DateTime _now;
    }
}