using System;
using System.Linq.Expressions;
using Moq;

namespace Avenue.Testing.Moq
{
    public class WithEventHandlersFromSome<T>
    {
        public static T With(Expression<Func<T, bool>> func)
        {
            return It.Is<T>(func);
        }

        public static T Like(T comparisonObject)
        {
            return It.Is<T>(x => x.IsEquivalentTo(comparisonObject));
        }
    }
}