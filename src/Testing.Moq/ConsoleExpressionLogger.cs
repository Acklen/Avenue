using System;

namespace Avenue.Testing.Moq
{
    public class ConsoleExpressionLogger : IExpressionLogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}