namespace Avenue.Testing.Moq
{
    public interface IExpressionLogger
    {
        void Log(string message);
    }
}