﻿using System.Threading.Tasks;
using Avenue.Dispatch;
using Avenue.Domain;

namespace Avenue.Commands.EntityFramework
{
    public class UnitOfWorkCommandDispatcherDecorator : IDispatcher<ICommand>
    {
        readonly IUnitOfWork _unitOfWork;

        public UnitOfWorkCommandDispatcherDecorator(IDispatcher<ICommand> decorated, IUnitOfWork unitOfWork)
        {
            DecoratedDispatcher = decorated;
            _unitOfWork = unitOfWork;
        }

        public IDispatcher<ICommand> DecoratedDispatcher { get; }

        public Task Dispatch(ICommand commandOrEvent)
        {
            return _unitOfWork.Execute(async () => await DecoratedDispatcher.Dispatch(commandOrEvent));
        }
    }
}