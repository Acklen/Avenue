using System;
using System.Threading.Tasks;
using Avenue.Commands;
using TestApp.Commands;

namespace TestApp
{
    public class TestValidator: ICommandValidator<ChangeName>{
        public Task Validate(ChangeName command)
        {
            Console.WriteLine("ChangeName validated.");
            return Task.CompletedTask;
        }
    }
}