using System;
using System.Collections.Generic;
using Avenue.EventSourcing;
using TestApp.Events;

namespace TestApp
{
    public class Person : AggregateRoot
    {
        public string Name { get; set; }

        public Person(Guid id, string name) : base(new List<object>())
        {
            When(NewChange(new PersonCreated(id, name)));
        }

        void When(PersonCreated newChange)
        {
            Id = newChange.Id;
            Name = newChange.Name;
        }

        public Person(IEnumerable<object> domainEvents) : base(domainEvents)
        {
        }

        public void ChangeName(string newName)
        {
            When(NewChange(new NameChanged(Id, newName)));
        }

        void When(NameChanged @event)
        {
            Name = @event.Name;
        }
    }
}