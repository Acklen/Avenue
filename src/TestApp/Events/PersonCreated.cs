using System;
using Avenue.Domain;

namespace TestApp.Events
{
    public class PersonCreated : IDomainEvent
    {
        public Guid Id { get; }
        public string Name { get; }

        public PersonCreated(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}