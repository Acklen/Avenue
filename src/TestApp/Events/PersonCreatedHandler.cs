using System.Threading.Tasks;
using Avenue.Domain;
using Avenue.Events;

namespace TestApp.Events
{
    public class PersonCreatedHandler : IEventHandler<PersonCreated>{
        
        readonly IWritableRepository<PeopleListing> _repo;

        public PersonCreatedHandler(IWritableRepository<PeopleListing> repo)
        {
            _repo = repo;
        }

        public async Task Handle(PersonCreated @event)
        {
            var peopleListing = new PeopleListing() {Id = @event.Id, Name = @event.Name};
            await _repo.Create(peopleListing);
        }
    }
}