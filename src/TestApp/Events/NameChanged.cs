using System;
using Avenue.Domain;

namespace TestApp.Events
{
    public class NameChanged : IDomainEvent
    {
        public Guid Id { get; }
        public string Name { get; }

        public NameChanged(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}