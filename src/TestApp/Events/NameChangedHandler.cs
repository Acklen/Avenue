using System.Threading.Tasks;
using Avenue.Domain;
using Avenue.Events;

namespace TestApp.Events
{
    public class NameChangedHandler : IEventHandler<NameChanged>
    {
        readonly IWritableRepository<PeopleListing> _repo;

        public NameChangedHandler(IWritableRepository<PeopleListing> repo)
        {
            _repo = repo;
        }

        public async Task Handle(NameChanged @event)
        {
            var peopleListing = await _repo.Find(@event.Id);
            peopleListing.Name = @event.Name;
            await _repo.Update(peopleListing);
        }
    }
}