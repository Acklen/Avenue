using System;
using Avenue.Commands;

namespace TestApp.Commands
{
    public class CreatePerson : ICommand
    {
        public Guid Id { get; }
        public string Name { get; }

        public CreatePerson(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}