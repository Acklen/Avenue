using System;
using Avenue.Commands;

namespace TestApp.Commands
{
    public class ChangeName : ICommand
    {
        public ChangeName(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
        
        public Guid Id { get; }
        public string Name { get; }
    }
}