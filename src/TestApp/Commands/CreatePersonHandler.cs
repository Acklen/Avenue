using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace TestApp.Commands
{
    public class CreatePersonHandler : ICommandHandler<CreatePerson>
    {
        readonly IWritableRepository<Person> _repo;

        public CreatePersonHandler(IWritableRepository<Person> repo)
        {
            _repo = repo;
        }
        
        public async Task Handle(CreatePerson command)
        {
            var aggregateRoot = new Person(command.Id, command.Name);
            await _repo.Create(aggregateRoot);
        }
    }
}