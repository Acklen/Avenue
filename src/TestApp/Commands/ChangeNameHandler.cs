using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace TestApp.Commands
{
    public class ChangeNameHandler : ICommandHandler<ChangeName>
    {
        readonly IWritableRepository<Person> _repo;

        public ChangeNameHandler(IWritableRepository<Person> repo)
        {
            _repo = repo;
        }
        
        public async Task Handle(ChangeName command)
        {
            var aggregateRoot = await _repo.Find(command.Id);
            aggregateRoot.ChangeName(command.Name);
            await _repo.Create(aggregateRoot);
        }
    }
}