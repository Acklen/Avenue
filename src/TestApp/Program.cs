﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Avenue.AspNetCore;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.EventSourcing.EntityFramework;
using Avenue.Persistence.EntityFramework;
using Avenue.Persistence.Relational;
using Avenue.Worker.Commands;
using Avenue.Worker.Events;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using TestApp.Commands;
using TestApp.Events;

namespace TestApp
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var domainEvents = new PubSub<IDomainEvent>();
            var commandQueue = new PubSub<ICommand>();

            Action<DbContextOptionsBuilder> commandWorkerDbOptions = options =>
            {
                options.UseSqlite("Data Source=test_eventSource.db");
            };

            Action<DbContextOptionsBuilder> eventWorkerDbOptions = options =>
            {
                options.UseSqlite("Data Source=test_projections.db");
            };

            Action<IServiceProvider> serviceInitializationBuilder = p =>
            {
                var dbContext = p.GetService<IDbContext>();
                dbContext.Database.EnsureCreated();
            };

            var hostServices = new ServiceCollection();
            
            //shared dbContext config for front-end and event dispatcher
            Action<DbContextConfig<CodeFirstDataContext>> dbContextConfigAction = db => db
                .WithDbOptions(eventWorkerDbOptions);
                
           

            var serviceProvider = hostServices.BuildServiceProvider();

            //simulate the application
            var localCommandDispatcher = serviceProvider.GetService<IDispatcher<ICommand>>();
            var personId = Guid.NewGuid();
            await localCommandDispatcher.Dispatch(new CreatePerson(personId, "Mickey"));
            await localCommandDispatcher.Dispatch(new ChangeName(personId, "Donald"));
            

            Console.ReadKey();
        }
    }
}