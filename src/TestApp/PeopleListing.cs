using System;
using System.Collections.Generic;
using Avenue.Domain;

namespace TestApp
{
    public class PeopleListing : IViewModel<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        
        public IEnumerable<IDomainEvent> GetChanges()
        {
            return new List<IDomainEvent>();
        }

        public void ClearChanges()
        {
        }
    }
}