using System.Collections.Generic;

namespace Avenue.Domain
{
    public interface IHaveDomainEvents
    {
        IEnumerable<IDomainEvent> GetChanges();
        void ClearChanges();
    }
}