using System;

namespace Avenue.Domain
{
    public interface ITimeProvider
    {
        DateTime GetNow();
    }
}