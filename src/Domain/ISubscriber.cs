using System;
using System.Threading.Tasks;

namespace Avenue.Domain
{
    public interface ISubscriber<out TItem> 
    {
        Task Subscribe(Action<TItem> onNewItem);
    }
}