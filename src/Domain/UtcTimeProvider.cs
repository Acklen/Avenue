using System;

namespace Avenue.Domain
{
    public class UtcTimeProvider : ITimeProvider
    {
        public DateTime GetNow()
        {
            return DateTime.UtcNow;
        }
    }
}