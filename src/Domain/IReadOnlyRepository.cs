using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avenue.Domain
{
    public interface IReadOnlyRepository<TDatabaseBackedObjectType>
    {
        IQueryable<TDatabaseBackedObjectType> Set();
        Task<TDatabaseBackedObjectType> GetById(object id);
        IQueryable<TDatabaseBackedObjectType> FindByCondition(IDictionary<string,string> parameters);
    }
}