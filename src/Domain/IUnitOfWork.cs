using System;
using System.Threading.Tasks;

namespace Avenue.Domain
{
    public interface IUnitOfWork
    {
        Task Execute(Func<Task> func);
    }
}