using System;
using System.Threading.Tasks;

namespace Avenue.Domain
{
    public interface IWritableRepository<TDatabaseBackedObjectType>  where TDatabaseBackedObjectType : class
    {
        Task<TDatabaseBackedObjectType> Find(Guid id);
        Task<TDatabaseBackedObjectType> Create(TDatabaseBackedObjectType entity);
        Task<TDatabaseBackedObjectType> Update(TDatabaseBackedObjectType entity);
    }
}