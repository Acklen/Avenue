namespace Avenue.Domain
{
    public interface IRemovable
    {
        bool Removed { get; }
    }
}