namespace Avenue.Domain
{
    public interface IDomainEvent
    {
    }
    
    public enum RegistrationPriority
    {
        RunFirst = 0,
        RunWhenever = 50,
        RunLast = 100
    }
}