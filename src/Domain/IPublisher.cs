using System.Threading.Tasks;

namespace Avenue.Domain
{
    public interface IPublisher<in TItemToPublish>
    {
        Task Publish(TItemToPublish item);
    }
}