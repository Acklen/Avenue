using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;

namespace Avenue.Domain
{
    public class PubSub<T> : ISubscriber<T>, IPublisher<T>
    {
        readonly Subject<T> _jobs = new Subject<T>();
        readonly IConnectableObservable<T> _connectableObservable;
 
        public PubSub()
        {
            _connectableObservable = _jobs.ObserveOn(Scheduler.Default).Publish();
            _connectableObservable.Connect();
        }
 
        public Task Subscribe(Action<T> onNewItem)
        {
            _connectableObservable.Subscribe(onNewItem);
            return Task.CompletedTask;
        }

        public Task Publish(T item)
        {
            _jobs.OnNext(item);
            return Task.CompletedTask;
        }
    }
}