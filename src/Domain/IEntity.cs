using System.Collections.Generic;

namespace Avenue.Domain
{
    public interface IEntity<out T> : IHaveDomainEvents
    {
        T Id { get; }
    }
}