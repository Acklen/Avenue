using Microsoft.EntityFrameworkCore;

namespace Avenue.Persistence.EntityFramework
{
    public interface IModelBuildingStrategy
    {
        void Invoke(ModelBuilder modelBuilder);
    }
}