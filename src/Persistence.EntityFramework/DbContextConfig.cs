using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.Persistence.EntityFramework
{
    public class DbContextConfig<TDbContext> where TDbContext : DbContext, IDbContext
    {
        IModelBuildingStrategy _modelBuildingStrategy;
        Action<DbContextOptionsBuilder> _dbOptionsBuilder;

        public DbContextConfig(IEnumerable<Type> entityTypes)
        {
            if (entityTypes.Any())
            {
                _modelBuildingStrategy = new DefaultModelBuildingStrategy(entityTypes);
            }
        }

        public DbContextConfig<TDbContext> WithDbOptions(Action<DbContextOptionsBuilder> dbOptionsBuilder)
        {
            _dbOptionsBuilder = dbOptionsBuilder;
            return this;
        }

        public DbContextConfig<TDbContext> WithModelBuildingStrategy(IModelBuildingStrategy modelBuildingStrategy)
        {
            _modelBuildingStrategy = modelBuildingStrategy;
            return this;
        }

        public void Apply(IServiceCollection serviceCollection)
        {
            serviceCollection.AddDbContext<IDbContext, TDbContext>(_dbOptionsBuilder);
            
            if (_modelBuildingStrategy != null)
            {
                serviceCollection.AddSingleton(_modelBuildingStrategy);
            }

        }
    }
}