﻿using System;
using System.Threading.Tasks;
using Avenue.Domain;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.Persistence.EntityFramework
{
    public class DefaultUnitOfWork : IUnitOfWork
    {
        readonly IDbContext _dbContext;

        public DefaultUnitOfWork(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Execute(Func<Task> func)
        {
            using var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                await func.Invoke();
                await _dbContext.SaveChangesAsync();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new UnitOfWorkException(_dbContext.GetType(), ex);
            }
        }
    }
}