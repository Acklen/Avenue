using System;

namespace Avenue.Persistence.EntityFramework
{
    public class UnitOfWorkException : Exception
    {
        public UnitOfWorkException(Type dbContextType, Exception innerException):base($"Could not commit changes to {dbContextType}. Changes have been rolled back.", innerException)
        {
        }
    }
}