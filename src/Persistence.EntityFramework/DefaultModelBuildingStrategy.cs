using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Avenue.Persistence.EntityFramework
{
    public class DefaultModelBuildingStrategy : IModelBuildingStrategy
    {
        readonly IEnumerable<Type> _entityTypes;

        public DefaultModelBuildingStrategy(IEnumerable<Type> entityTypes)
        {
            _entityTypes = entityTypes.ToList();
            if(!_entityTypes.Any()) throw new ArgumentException("Must have at least one entity type.", nameof(entityTypes));
        }

        public void Invoke(ModelBuilder modelBuilder)
        {
            foreach (var entity in _entityTypes) modelBuilder.Entity(entity);
        }
    }
}