﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Avenue.Domain;
using Avenue.Events;

namespace Avenue.EventSourcing
{
    public interface IEventStore : IEventStore<IDomainEvent>
    {
        
    }
    public interface IEventStore<TDomainEvent>
    {
        Task<IEnumerable<TDomainEvent>> Get<TAggregateRoot>(Guid aggregateId);
        Task Write<TAggregateRoot>(Guid aggregateId, IEnumerable<TDomainEvent> @event);
    }
}