using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.Events;

namespace Avenue.EventSourcing
{
    public class
        DispatchingAggregateRootRepository<TAggregateRootType> : IWritableRepository<TAggregateRootType> where TAggregateRootType : class, IAggregateRoot
    {
        readonly IWritableRepository<TAggregateRootType> _parentRepository;
        readonly IPublisher<IDomainEvent> _publisher;

        public DispatchingAggregateRootRepository(IWritableRepository<TAggregateRootType> parentRepository,
            IPublisher<IDomainEvent> publisher)
        {
            _parentRepository = parentRepository;
            _publisher = publisher;
        }

        public Task<TAggregateRootType> Find(Guid id)
        {
            return _parentRepository.Find(id);
        }

        public async Task<TAggregateRootType> Create(TAggregateRootType entity)
        {
            var events = entity.GetChanges();
            await _parentRepository.Create(entity);
            await PublishEvents(events);
            return entity;
        }

        async Task PublishEvents(IEnumerable<IDomainEvent> events)
        {
            foreach (var ev in events)
                await _publisher.Publish(ev);
        }

        public async Task<TAggregateRootType> Update(TAggregateRootType entity)
        {
            IEnumerable<IDomainEvent> events = entity.GetChanges();
            await _parentRepository.Update(entity);
            await PublishEvents(events);
            return entity;
        }
    }
}