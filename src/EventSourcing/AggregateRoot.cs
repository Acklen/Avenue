﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Avenue.Domain;
using Avenue.Events;

namespace Avenue.EventSourcing
{
    public abstract class AggregateRoot : IAggregateRoot
    {
        readonly List<IDomainEvent> _changes = new List<IDomainEvent>();

        protected AggregateRoot(IEnumerable<object> domainEvents)
        {
            foreach (var e in domainEvents) Mutate(e);
        }

        void Mutate(object domainEvent)
        {
            var applyMethod = GetMethodForDomainEvent(domainEvent);
            applyMethod.Invoke(this, new[] {domainEvent});
        }

        MethodInfo GetMethodForDomainEvent(object domainEvent)
        {
            IEnumerable<MethodInfo> methodInfos = GetType()
                .GetMethods(BindingFlags.NonPublic | BindingFlags.Instance)
                .Where(x => x.Name == "When").ToList();

            var applyMethod =
                methodInfos.FirstOrDefault(x =>
                {
                    var parameterInfos =
                        x.GetParameters();

                    return parameterInfos.First().ParameterType ==
                           domainEvent.GetType();
                });

            if (applyMethod == null)
            {
                var validDomainEventTypes = methodInfos.Select(x => x.GetParameters()[0].ParameterType.Name);
                throw new CannotMutateAggregateWithUnknownEventException(GetType().Name, domainEvent.GetType().Name,
                    validDomainEventTypes);
            }

            return applyMethod;
        }

        protected T NewChange<T>(T @event) where T: IDomainEvent
        {
            _changes.Add(@event);
            return @event;
        }

        public Guid Id { get; protected set; }

        public IEnumerable<IDomainEvent> GetChanges()
        {
            var domainEvents = new List<IDomainEvent>();
            domainEvents.AddRange(_changes);
            return domainEvents;
        }

        public void ClearChanges()
        {
            _changes.Clear();
        }
    }
}