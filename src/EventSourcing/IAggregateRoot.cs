using System;
using Avenue.Domain;

namespace Avenue.EventSourcing
{
    public interface IAggregateRoot: IHaveDomainEvents
    {
        Guid Id { get; }
    }
}