using System;
using System.Collections.Generic;

namespace Avenue.EventSourcing
{
    public class CannotMutateAggregateWithUnknownEventException : Exception
    {
        public CannotMutateAggregateWithUnknownEventException(string aggregateTypeName, string domainEventTypeName,
            IEnumerable<string> validDomainEventTypeNames)
            : base(
                $"The current aggregate version of '{aggregateTypeName}' does not support the provided domain event type '{domainEventTypeName}'. These are the supported domain event types: {string.Join(", ", validDomainEventTypeNames)}. (NOTE: There must be a 'When' method for each domain event)")
        {
        }
    }
}