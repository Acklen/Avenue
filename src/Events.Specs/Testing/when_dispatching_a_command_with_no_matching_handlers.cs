﻿using System;
using System.Collections.Generic;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.Events.Specs.Stubs;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Avenue.Events.Specs.Testing
{
    public class when_dispatching_an_event_with_no_matching_handlers
    {
        static IDispatcher<IDomainEvent> _dispatcher;
        static readonly TestEvent TestEvent = new TestEvent();
        static Exception _exception;

        Establish context =
            () =>
            {
                var commandHandlers = new List<IEventHandler>
                {
                    new AnotherTestEventHandler()
                };

                _decoratedDispatcher = Mock.Of<IDispatcher<IDomainEvent>>();
                _dispatcher = new HandlingEventDispatcherDecorator(_decoratedDispatcher, commandHandlers);
            };

        Because of =
            () => _exception = Catch.Exception(() => _dispatcher.Dispatch(TestEvent).Await());

        It should_not_throw_an_exception =
            () => _exception.ShouldBeNull();

        static IDispatcher<IDomainEvent> _decoratedDispatcher;
    }
}