﻿using System.Collections.Generic;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.Events.Specs.Stubs;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Avenue.Events.Specs.Testing
{
    public class when_dispatching_an_event
    {
        static TestCommandHandler _testCommandHandler;
        static readonly TestEvent TestEvent = new TestEvent();

        static HandlingEventDispatcherDecorator _dispatcherDecorator;

        Establish context =
            () =>
            {
                _testCommandHandler = new TestCommandHandler();
                var commandHandlers = new List<IEventHandler>
                {
                    _testCommandHandler,
                    new AnotherTestEventHandler()
                };

                _decoratedDispatcher = Mock.Of<IDispatcher<IDomainEvent>>();
                _dispatcherDecorator = new HandlingEventDispatcherDecorator(_decoratedDispatcher, commandHandlers);
            };

        Because of =
            () => _dispatcherDecorator.Dispatch(TestEvent).Await();

        It should_dispatch_the_expected_command =
            () => _testCommandHandler.EventHandled.ShouldEqual(TestEvent);

        It should_dispatch_the_decorated_dispatcher =
            () => Mock.Get(_decoratedDispatcher).Verify(x => x.Dispatch(TestEvent));

        static IDispatcher<IDomainEvent> _decoratedDispatcher;
    }
}