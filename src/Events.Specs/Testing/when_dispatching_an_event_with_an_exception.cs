﻿using System;
using System.Collections.Generic;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.Events.Specs.Stubs;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Avenue.Events.Specs.Testing
{
    public class when_dispatching_an_event_with_an_exception
    {
        static IDispatcher<IDomainEvent> _dispatcher;
        static readonly TestEvent TestEvent = new TestEvent();
        static Exception _exception;
        static Exception _exceptionToThrow;

        Establish context =
            () =>
            {
                _exceptionToThrow = new Exception("Test");
                var commandHandlers = new List<IEventHandler>
                {
                    new TestCommandHandlerWithException().SetException(_exceptionToThrow)
                };
                
                _decoratedDispatcher = Mock.Of<IDispatcher<IDomainEvent>>();
                _dispatcher = new HandlingEventDispatcherDecorator(_decoratedDispatcher, commandHandlers);
            };

        Because of =
            () => _exception = Catch.Exception(() => _dispatcher.Dispatch(TestEvent).Await());

        It should_throw_a_not_implemented_exception =
            () => _exception.InnerException.ShouldEqual(_exceptionToThrow);

        static IDispatcher<IDomainEvent> _decoratedDispatcher;
    }
}