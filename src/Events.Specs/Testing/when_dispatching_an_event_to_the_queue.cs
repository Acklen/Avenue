using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.Events.Specs.Stubs;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Avenue.Events.Specs.Testing
{
    public class when_dispatching_an_event_to_the_queue
    {
        static IDispatcher<IDomainEvent> _dispatcher;
        static readonly TestEvent TestEvent = new TestEvent();
        static IPublisher<IDomainEvent> _publisher;

        Establish context =
            () =>
            {
                _publisher = Mock.Of<IPublisher<IDomainEvent>>();
                _dispatcher = new QueuedDomainEventDispatcher(_publisher);
            };

        Because of =
            () => _dispatcher.Dispatch(TestEvent).Await();

        It should_publish_the_event =
            () => Mock.Get(_publisher).Verify(x => x.Publish(TestEvent));
    }
}