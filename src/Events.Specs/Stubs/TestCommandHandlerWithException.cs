﻿using System;
using System.Threading.Tasks;
using Avenue.Dispatch;

namespace Avenue.Events.Specs.Stubs
{
    public class TestCommandHandlerWithException : IEventHandler<TestEvent>
    {
        Exception _exceptionToThrow;

        public TestCommandHandlerWithException SetException(Exception exceptionToThrow)
        {
            _exceptionToThrow = exceptionToThrow;
            return this;
        }
        
        public Task Handle(TestEvent @event)
        {
            throw _exceptionToThrow;
        }
    }
}