﻿using System.Threading.Tasks;
using Avenue.Dispatch;

namespace Avenue.Events.Specs.Stubs
{
    public class TestCommandHandler : IEventHandler<TestEvent>
    {
        public TestEvent EventHandled { get; private set; }

        public Task Handle(TestEvent @event)
        {
            EventHandled = @event;
            return Task.CompletedTask;
        }
    }
}