﻿using Avenue.Domain;

namespace Avenue.Events.Specs.Stubs
{
    public class TestEvent : IDomainEvent
    {
        public TestEvent()
        {
        }

        public TestEvent(object command)
        {
            Command = command;
        }

        public object Command { get; private set; }
    }
}