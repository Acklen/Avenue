﻿using System.Threading.Tasks;
using Avenue.Dispatch;

namespace Avenue.Events.Specs.Stubs
{
    public class AnotherTestEventHandler : IEventHandler<AnotherTestEvent>
    {
        public AnotherTestEventHandler()
        {
            
        }
        public AnotherTestEvent EventHandled { get; private set; }

        public Task Handle(AnotherTestEvent @event)
        {
            EventHandled = @event;
            return Task.CompletedTask;
        }
    }
}