using System;
using Avenue.Domain;
using FluentAssertions;
using Machine.Specifications;
using Xunit;

namespace Domain.Specs
{
    public class when_getting_utc_now
    {
        Establish _context = () => { _systemUnderTest = new UtcTimeProvider(); };

        Because of = () => { _result = _systemUnderTest.GetNow(); };

        It should_return_utc_now = () =>
        {
            _result.ToLongDateString().Should().Be(DateTime.UtcNow.ToLongDateString());
        };

        static UtcTimeProvider _systemUnderTest;
        static DateTime _result;
    }
}