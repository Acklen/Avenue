using Avenue.Commands;
using Avenue.Commands.AspNetCore;
using Avenue.Commands.EntityFramework;
using Avenue.Commands.EntityFramework.AspNetCore;
using Avenue.Domain;
using Avenue.Testing;
using Avenue.Testing.AspNetCore;
using Machine.Specifications;
using Microsoft.Extensions.DependencyInjection;

namespace Commands.EntityFramework.AspNetCore.Specs
{
    public class when_configuring_aspnet_to_commands_with_a_unit_of_work
    {
        Establish _context = () =>
        {
            _systemUnderTest = new ServiceCollection();
            _systemUnderTest.AddTransient<IUnitOfWork, TestUnitOfWork>();
        };

        Because of = () => { _systemUnderTest.WithCommands(config => config
            .WithImmediateDispatch()
            .WithUnitOfWork()); };

        It should_be_able_to_resolve_the_command_dispatcher = () =>
        {
            _systemUnderTest
                .ShouldResolve<ICommandDispatcher>()
                .ThatIsA<UnitOfWorkCommandDispatcher>()
                .DecoratedDispatcher
                .ThatIsA<WorkerCommandDispatcher>()
                .DecoratedDispatcher
                .ThatIsA<ValidatedCommandDispatcher>();
        };

        static IServiceCollection _systemUnderTest;
    }
}