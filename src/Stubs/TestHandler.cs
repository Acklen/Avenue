using System.Threading.Tasks;
using Avenue.Commands;

namespace Stubs
{
    public class TestHandler : ICommandHandler<TestCommand>
    {
        public Task Handle(TestCommand command)
        {
            CommandHandled = command;
            return Task.CompletedTask;
        }

        public object CommandHandled { get; set; }
    }
}