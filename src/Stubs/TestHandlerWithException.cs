﻿using System;
using System.Threading.Tasks;
using Avenue.Commands;

namespace Stubs
{
    public class TestHandlerWithException : ICommandHandler<TestCommand>
    {
        Exception _exceptionToThrow;

        public TestHandlerWithException SetException(Exception exceptionToThrow)
        {
            _exceptionToThrow = exceptionToThrow;
            return this;
        }
        
        public Task Handle(TestCommand command)
        {
            throw _exceptionToThrow;
        }
    }
}