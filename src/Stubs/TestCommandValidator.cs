using System.Threading.Tasks;
using Avenue.Commands;

namespace Stubs
{
    public class TestCommandValidator : ICommandValidator<TestCommand>, ICommandValidator<TestCommand2>
    {
        public TestCommand CommandValidated { get; private set; }
        public TestCommand2 Command2Validated { get; private set; }

        public Task Validate(TestCommand command)
        {
            CommandValidated = command;
            return Task.CompletedTask;
        }

        public Task Validate(TestCommand2 command)
        {
            Command2Validated = command;
            return Task.CompletedTask;
        }
    }
}