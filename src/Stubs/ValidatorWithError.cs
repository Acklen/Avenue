using System;
using System.Threading.Tasks;
using Avenue.Commands;

namespace Stubs
{
    public class ValidatorWithError : ICommandValidator<TestCommand>
    {
        public Task Validate(TestCommand command)
        {
            throw new Exception("Validation error");
        }
    }
}