﻿using System;
using System.Collections.Generic;

namespace TypeScanning
{
    public interface ITypeScanner
    {
        List<Type> GetTypesOf(Type baseType);
        List<Type> GetTypesOf<TBaseType>();
        Type GetOneType(string baseType);
    }
}