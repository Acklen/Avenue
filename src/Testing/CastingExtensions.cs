﻿using System;

namespace Avenue.Testing
{
    public static class CastingExtensions
    {
        public static T ThatIsA<T>(this object obj)
        {
            return CastAs<T>(obj);
        }

        public static T CastAs<T>(this object obj)
        {
            try
            {
                return (T) obj;
            }
            catch (Exception)
            {
                throw new Exception(
                    $"That object is of type '{obj.GetType().Name}' and cannot be cast as a '{typeof(T).Name}'.");
            }
        }
    }
}