﻿using System.Threading.Tasks;

namespace Avenue.Testing
{
    public class AwaitResult
    {
        readonly Task _task;

        public AwaitResult(Task task)
        {
            _task = task;
        }

        public Task AsTask => _task;
    }

    public class AwaitResult<T>
    {
        readonly Task<T> _task;

        public AwaitResult(Task<T> task)
        {
            _task = task;
        }

        public Task<T> AsTask => _task;

        public static implicit operator T(AwaitResult<T> m)
        {
            return m._task.Result;
        }
    }
}