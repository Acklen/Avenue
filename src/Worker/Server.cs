using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace Avenue.Worker
{
    public interface IAvenueServer
    {
        Task StartAsync(CancellationToken cancellationToken);
        Task StopAsync(CancellationToken cancellationToken);
    }
}