using Microsoft.Extensions.DependencyInjection;

namespace Avenue.Worker
{
    public interface IDispatcherServiceCollection : IServiceCollection
    {
    }
}