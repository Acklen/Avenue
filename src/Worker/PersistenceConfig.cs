using System;
using System.Collections.Generic;
using System.Linq;
using Avenue.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.Worker
{
    public class PersistenceConfig
    {
        readonly List<Type> _entityTypes;

        public PersistenceConfig()
        {
            ServiceConfigurations = new Queue<Action<IServiceCollection>>();
            _entityTypes = new List<Type>();
        }

        public IEnumerable<Type> EntityTypes => _entityTypes;

        public Queue<Action<IServiceCollection>> ServiceConfigurations { get; private set; }

        public void Apply(IServiceCollection serviceCollection)
        {
            while (ServiceConfigurations.TryDequeue(out var action))
                action.Invoke(serviceCollection);
        }

        public PersistenceConfig AddDbContext<TDbContext>(Action<DbContextConfig<TDbContext>> dbContextConfigAction)
            where TDbContext : DbContext, IDbContext
        {
            if (!EntityTypes.Any())
            {
                throw new Exception("No entity types were defined before setting up the db context.");
            }

            var dbContextConfig = new DbContextConfig<TDbContext>(EntityTypes);
            dbContextConfigAction.Invoke(dbContextConfig);
            ServiceConfigurations.Enqueue(x => dbContextConfig.Apply(x));
            return this;
        }

        public PersistenceConfig WithGenericEntityRepository(Action<GenericRepositoryConfig> configAction = null)
        {
            var genericRepositoryConfig = new GenericRepositoryConfig(EntityTypes);
            configAction?.Invoke(genericRepositoryConfig);
            genericRepositoryConfig.Execute(this);
            return this;
        }

        public PersistenceConfig WithEntityTypes(params Type[] types)
        {
            _entityTypes.AddRange(types.Where(t => _entityTypes.All(x => x != t)));
            return this;
        }
    }
}