using System;
using System.Collections.Generic;
using Avenue.AspNetCore.Extensions;
using Avenue.Domain;
using Avenue.Persistence.Relational;

namespace Avenue.Worker
{
    public class GenericRepositoryConfig
    {
        readonly IEnumerable<Type> _entityTypes;

        public GenericRepositoryConfig(IEnumerable<Type> entityTypes)
        {
            _entityTypes = entityTypes;
        }

        public void Execute(PersistenceConfig config)
        {
            config.ServiceConfigurations.Enqueue(services =>
            {
                services.AddForEachType(typeof(IReadOnlyRepository<>), typeof(ReadOnlyEntityRepository<>), _entityTypes);
                
                if (WritableRepository)
                    services.AddForEachType(typeof(IWritableRepository<>), typeof(WritableEntityRepository<>), _entityTypes);
                
            });
        }

        

        bool WritableRepository { get; set; }

        public GenericRepositoryConfig IncludeWritableRepository()
        {
            WritableRepository = true;
            return this;
        }
    }
}