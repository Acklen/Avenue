using System;
using System.Collections.Generic;
using Avenue.Persistence.EntityFramework;
using Avenue.Persistence.Relational;
using FizzWare.NBuilder;
using Microsoft.EntityFrameworkCore;

namespace Data.Specs.Integration.Repositories
{
    public static class InMemoryDataContext
    {
        public static IDbContext GetInMemoryContext<T>()
        {
            var options = new DbContextOptionsBuilder<CodeFirstDataContext>()
                .UseInMemoryDatabase("test")
                .Options;

            var appDataContext = new CodeFirstDataContext(options, new List<IModelBuildingStrategy>
            {
                new DefaultModelBuildingStrategy(new[]{typeof(T)})
            });

            return appDataContext;
        }

        public static IDbContext Seed<T>(this IDbContext appDataContext) where T : class
        {
            var set = appDataContext.Set<T>();
            var projects = Builder<T>.CreateListOfSize(10)
                .Build();
            set.AddRange(projects);
            appDataContext.SaveChangesAsync().Wait();
            return appDataContext;
        }

        public static void Clean(this IDbContext appDataContext)
        {
            appDataContext.Database.EnsureDeleted();
        }
    }
}