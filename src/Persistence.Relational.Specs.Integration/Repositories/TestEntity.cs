using System;
using Avenue.Domain;

namespace Data.Specs.Integration.Repositories
{
    public class TestEntity : IRemovable
    {
        public string LastName { get; set; }
        public Guid Id { get; set; }
        public bool Removed { get; set; }
    }
}