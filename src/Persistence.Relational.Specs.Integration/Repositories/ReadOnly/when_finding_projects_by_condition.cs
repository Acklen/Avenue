using System;
using System.Collections.Generic;
using System.Linq;
using Avenue.Domain;
using Avenue.Persistence.EntityFramework;
using Avenue.Persistence.Relational;
using FluentAssertions;
using Machine.Specifications;

namespace Data.Specs.Integration.Repositories.ReadOnly
{
    public class when_finding_projects_by_condition
    {
        static IReadOnlyRepository<TestEntity> _systemUnderTest;
        static IEnumerable<TestEntity> _result;
        static string _lastName;
        static TestEntity _expectedTestEntity;
        static IDbContext _appDataContext;

        Establish _context = () =>
        {
            _appDataContext = InMemoryDataContext.GetInMemoryContext<TestEntity>().Seed<TestEntity>();

            _systemUnderTest = new ReadOnlyEntityRepository<TestEntity>(_appDataContext);

            _expectedTestEntity = _appDataContext.Set<TestEntity>().First();

            _lastName = _expectedTestEntity.LastName;
        };

        Cleanup after = () => { _appDataContext.Clean(); };

        Because of = () => { _result = _systemUnderTest.FindByCondition(new Dictionary<string, string>{{ "LastName", _lastName}}); };

        It should_return_the_matching_projects = () => { _result.ToList().First().Should().BeEquivalentTo(_expectedTestEntity); };
    }
}