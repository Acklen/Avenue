using System;
using System.Linq;
using Avenue.Persistence.EntityFramework;
using Avenue.Persistence.Relational;
using FluentAssertions;
using Machine.Specifications;

namespace Data.Specs.Integration.Repositories.ReadOnly
{
    public class when_getting_one_project_by_id_from_the_read_only_repository
    {
        static ReadOnlyEntityRepository<TestEntity> _systemUnderTest;
        static TestEntity _result;
        static TestEntity _testEntity;
        static IDbContext _appDataContext;

        Cleanup after = () => { _appDataContext.Clean(); };

        Establish context = () =>
        {
            _appDataContext = InMemoryDataContext
                .GetInMemoryContext<TestEntity>()
                .Seed<TestEntity>();

            _testEntity = _appDataContext.Set<TestEntity>().ToList()[2];
            _systemUnderTest = new ReadOnlyEntityRepository<TestEntity>(_appDataContext);
        };

        Because of = async () => { _result = await _systemUnderTest.GetById(_testEntity.Id); };

        It should_return_the_selected_project = () =>
            _result.Should().BeEquivalentTo(_testEntity);
    }
}