using System;
using System.Collections.Generic;
using System.Linq;
using Avenue.Domain;
using Avenue.Persistence.EntityFramework;
using Avenue.Persistence.Relational;
using FluentAssertions;
using Machine.Specifications;
using Microsoft.EntityFrameworkCore;

namespace Data.Specs.Integration.Repositories.ReadOnly
{
    public class when_getting_non_removed_projects_from_the_read_only_repository
    {
        static IReadOnlyRepository<TestEntity> _systemUnderTest;
        static IEnumerable<TestEntity> _result;
        static IEnumerable<TestEntity> _nonRemovedTestEntities;
        static IDbContext _appDataContext;

        Cleanup after = () => { _appDataContext.Clean(); };

        Establish context = () =>
        {
            _appDataContext = InMemoryDataContext
                .GetInMemoryContext<TestEntity>()
                .Seed<TestEntity>();

            _nonRemovedTestEntities = _appDataContext.Set<TestEntity>().Where(x => !x.Removed);
            _systemUnderTest = new ReadOnlyEntityRepository<TestEntity>(_appDataContext);
        };

        Because of = async () => { _result = await _systemUnderTest.Set().ToListAsync(); };

        It should_return_a_list_of_non_removed_projects = () =>
            _result.Should().BeEquivalentTo(_nonRemovedTestEntities);
    }
}