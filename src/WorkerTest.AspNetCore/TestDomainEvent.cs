using Avenue.Domain;

namespace WorkerTest.AspNetCore
{
    public class TestDomainEvent : IDomainEvent
    {
        public string Message { get; }

        public TestDomainEvent(string message)
        {
            Message = message;
        }
    }
}