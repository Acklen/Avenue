using System;
using System.Threading.Tasks;
using Avenue.Commands;

namespace WorkerTest.AspNetCore
{
    public class TestCommandHandler : ICommandHandler<TestCommand>
    {
        public Task Handle(TestCommand command)
        {
            Console.WriteLine(command.Message);
            return Task.CompletedTask;
        }
    }
}