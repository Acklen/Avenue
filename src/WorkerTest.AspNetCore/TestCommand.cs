using Avenue.Commands;

namespace WorkerTest.AspNetCore
{
    public class TestCommand : ICommand
    {
        public string Message { get; }

        public TestCommand(string message)
        {
            Message = message;
        }
    }
}