using System;
using System.Threading.Tasks;
using Avenue.Domain;
using Avenue.Events;

namespace WorkerTest.AspNetCore
{
    public class TestEventHandler : IEventHandler<TestDomainEvent>
    {
        readonly IWritableRepository<TestEntity> _repo;

        public TestEventHandler(IWritableRepository<TestEntity> repo)
        {
            _repo = repo;
        }
        public async Task Handle(TestDomainEvent @event)
        {
            await _repo.Create(new TestEntity(Guid.NewGuid()));
            Console.WriteLine(@event.Message);
        }
    }
}