﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Avenue.AspNetCore;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.EventSourcing.EntityFramework;
using Avenue.Persistence.EntityFramework;
using Avenue.Persistence.Relational;
using Avenue.Worker.Commands;
using Avenue.Worker.Events;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Worker.AspNetCore;
using ICommand = Avenue.Commands.ICommand;

namespace WorkerTest.AspNetCore
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();

            //await SimpleServer(serviceCollection);
            var commandQueue = new PubSub<ICommand>();
            //await DispatcherServer(domainEvents, serviceCollection);
            //await CommandDispatchServer(domainEvents, serviceCollection);

            Action<DbContextConfig<CodeFirstDataContext>> dbContextConfigAction = db => db
                .WithDbOptions(opt => { opt.UseSqlite("Data Source=test_viewmodel.db"); });
            
            LocalDispatcher(serviceCollection, commandQueue, dbContextConfigAction);
            
            await CommandDispatchServerWithEventSourcing(commandQueue, serviceCollection);
            var domainEventQueue = new PubSub<IDomainEvent>();
            await EventDispatchServerWithViewModel(domainEventQueue, serviceCollection, dbContextConfigAction);
            await commandQueue.Publish(new TestCommand("Hi!"));
            await domainEventQueue.Publish(new TestDomainEvent("Howdy"));
            
            new Timer(async _ =>
            {
                var provider = serviceCollection.BuildServiceProvider();
                var dispatcher = provider.GetService<IDispatcher<ICommand>>();
                var testCommand = new TestCommand($"Hi from thread {Thread.CurrentThread.ManagedThreadId}!");
                Console.WriteLine("Publishing test command.");
                await dispatcher.Dispatch(testCommand);
            }, null, 0, 2000);

            Thread.Sleep(TimeSpan.FromMinutes(1));
        }

        static void LocalDispatcher(IServiceCollection hostServices, IPublisher<ICommand> commandQueue,
            Action<DbContextConfig<CodeFirstDataContext>> dbContextConfigAction)
        {
             //set up the local dispatcher for use in controllers
                        hostServices
                            .AddLocalCommandDispatcher(commandQueue,
                                config => config
                                    .WithCommandValidatorsInAssemblies(typeof(TestCommandValidator).Assembly)
                                    .WithLogging<QuietDispatcherLogger>())
                            .WithPersistenceConfiguration(persistence =>
                                persistence
                                    .WithEntityTypes(typeof(TestEntity))
                                    .AddDbContext(dbContextConfigAction)
                                    .WithGenericEntityRepository());
        }

        static async Task CommandDispatchServerWithEventSourcing(ISubscriber<ICommand> commandQueue,
            IServiceCollection serviceCollection)
        {
            IPublisher<IDomainEvent> domainEventQueue = new PubSub<IDomainEvent>();

            serviceCollection.AddDispatchServer(commandQueue, config => config
                .WithLogging<QuietDispatcherLogger>()
                .WithCommandHandlersFrom(typeof(TestCommandHandler).Assembly)
                .WithPersistenceConfiguration(persistence =>
                {
                    persistence
                        .WithWritableAggregateRootRepository<TestAggregate>()
                        .AddDbContext<EventSourcingDbContext>(db => db
                            .WithDbOptions(opt => { opt.UseSqlite("Data Source=test_ES_server.db"); })
                        )
                        .WithDomainEventDispatch(domainEventQueue)
                        .WithCommandUnitOfWork();
                })
                .WithDependencies(services =>
                {
                    //config for dispatcher, dispatcher decorators, handlers (scan), dependencies
                })
                .WithInitialization(p =>
                {
                    var dbContext = p.GetService<IDbContext>();
                    dbContext.Database.EnsureCreated();
                }));

            await StartServer(serviceCollection);
        }

        
        static async Task EventDispatchServerWithViewModel(ISubscriber<IDomainEvent> domainEvents,
            IServiceCollection serviceCollection, Action<DbContextConfig<CodeFirstDataContext>> dbContextConfigAction)
        {
            serviceCollection.AddDispatchServer(domainEvents, config => config
                .WithLogging<QuietDispatcherLogger>()
                .WithEventHandlersFrom(typeof(TestEventHandler).Assembly)
                .WithPersistenceConfiguration(persistence =>
                {
                    persistence
                        .WithEntityTypes(typeof(TestEntity))
                        .AddDbContext(dbContextConfigAction)
                        .WithGenericEntityRepository(x => x.IncludeWritableRepository())
                        .WithDomainEventUnitOfWork();
                })
                .WithDependencies(services =>
                {
                    //config for dispatcher, dispatcher decorators, handlers (scan), dependencies
                })
                .WithInitialization(p =>
                {
                    var dbContext = p.GetService<IDbContext>();
                    dbContext.Database.EnsureCreated();
                }));

            var commandQueue = new PubSub<ICommand>();
            serviceCollection
                .AddLocalCommandDispatcher(commandQueue)
                .WithPersistenceConfiguration(persistence =>
                    persistence
                        .WithEntityTypes(typeof(TestEntity))
                        .AddDbContext(dbContextConfigAction)
                        .WithGenericEntityRepository());

            var provider = serviceCollection.BuildServiceProvider();
            var dispatcher = provider.GetService<IDispatcher<ICommand>>();
            await dispatcher.Dispatch(new TestCommand("local dispatcher"));

            await commandQueue.Subscribe(x => Console.WriteLine(x));

            
            var readOnlyRepository = provider.GetService<IReadOnlyRepository<TestEntity>>();
            var testEntities = await readOnlyRepository.Set().ToListAsync();
            testEntities.ForEach(x=> Console.WriteLine(x.Id));

            await StartServer(serviceCollection);
        }

        
        static async Task CommandDispatchServer(ISubscriber<ICommand> commandQueue,
            IServiceCollection serviceCollection)
        {
            serviceCollection.AddDispatchServer(commandQueue, config => config
                .WithCommandHandlersFrom(typeof(TestCommandHandler).Assembly)
                .WithCommandValidatorsFrom(typeof(TestCommandValidator).Assembly)
                .WithDependencies(services =>
                {
                    //config for dispatcher, dispatcher decorators, handlers (scan), dependencies
                    
                }));

            await StartServer(serviceCollection);
        }

        static async Task DispatcherServer<T>(ISubscriber<T> commandQueue, IServiceCollection serviceCollection)
        {
            serviceCollection.AddDispatchServer(commandQueue,
                config => config
                    .WithDependencies(services =>
                    {
                        //config for dispatcher, dispatcher decorators, handlers (scan), dependencies
                        services.AddTransient(typeof(IDispatcher<T>), typeof(TestDispatcher<T>));
                    }));

            await StartServer(serviceCollection);
        }

        static async Task SimpleServer(ServiceCollection serviceCollection)
        {
            serviceCollection.AddServer(
                services => { },
                provider =>
                {
                    //provider.GetService<ILogger>().Log("It worked!");
                    return Task.CompletedTask;
                });

            await StartServer(serviceCollection);
        }

        static async Task StartServer(IServiceCollection serviceCollection)
        {
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var hostedService = serviceProvider.GetService<IHostedService>();
            await hostedService.StartAsync(new CancellationToken());
        }
    }
}