using System;
using System.Threading.Tasks;
using Avenue.Commands;

namespace WorkerTest.AspNetCore
{
    internal class TestCommandValidator : ICommandValidator<TestCommand>
    {
        public Task Validate(TestCommand command)
        {
            Console.WriteLine($"{command.Message}: Validated");
            return Task.CompletedTask;
        }
    }
}