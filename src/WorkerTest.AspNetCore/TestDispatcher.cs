using System;
using System.Threading.Tasks;
using Avenue.Dispatch;

namespace WorkerTest.AspNetCore
{
    internal class TestDispatcher<T> : IDispatcher<T>
    {
        public Task Dispatch(T commandOrEvent)
        {
            Console.WriteLine(commandOrEvent.GetType().ToString());
            return Task.CompletedTask;
        }
    }
}