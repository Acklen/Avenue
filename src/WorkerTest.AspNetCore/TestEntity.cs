using System;
using System.Collections.Generic;
using Avenue.Domain;

namespace WorkerTest.AspNetCore
{
    public class TestEntity : IRemovable, IEntity<Guid>
    {
        public TestEntity(Guid id)
        {
            Id = id;
        }

        public IEnumerable<IDomainEvent> GetChanges()
        {
            return new List<IDomainEvent>();
        }

        public void ClearChanges()
        {
            //Test Pending
        }

        public bool Removed { get; set; }
        public Guid Id { get; set; }
    }
}