using System;
using System.Collections.Generic;
using Avenue.Domain;
using Avenue.EventSourcing;

namespace WorkerTest.AspNetCore
{
    internal class TestAggregate : IAggregateRoot
    {
        public IEnumerable<IDomainEvent> GetChanges()
        {
            return new List<IDomainEvent>();
        }

        public void ClearChanges()
        {
            //Test Pending
        }

        public Guid Id { get; }
    }
}