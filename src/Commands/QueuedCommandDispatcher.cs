using System.Threading.Tasks;
using Avenue.Dispatch;
using Avenue.Domain;

namespace Avenue.Commands
{
    public class QueuedCommandDispatcherDecorator : IDispatcher<ICommand>
    {
        IDispatcher<ICommand> DecoratedDispatcher { get; }
        readonly IPublisher<ICommand> _publisher;

        public QueuedCommandDispatcherDecorator(IDispatcher<ICommand> decoratedDispatcher, IPublisher<ICommand> publisher)
        {
            DecoratedDispatcher = decoratedDispatcher;
            _publisher = publisher;
        }

        public async Task Dispatch(ICommand commandOrEvent)
        {
            await DecoratedDispatcher.Dispatch(commandOrEvent);
            await _publisher.Publish(commandOrEvent);
        }
    }
}