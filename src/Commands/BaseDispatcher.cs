using System.ComponentModel.Design;
using System.Threading.Tasks;
using Avenue.Dispatch;

namespace Avenue.Commands
{
    public class BaseCommandDispatcher : IDispatcher<ICommand>
    {
        public Task Dispatch(ICommand commandOrEvent)
        {
            return Task.CompletedTask;
        }
    }
}