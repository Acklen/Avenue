using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avenue.Dispatch;

namespace Avenue.Commands
{
    public class CommandHandlingDispatcherDecorator : IDispatcher<ICommand>
    {
        readonly IDispatcher<ICommand> _dispatcher;

        public CommandHandlingDispatcherDecorator(IDispatcher<ICommand> dispatcher,
            IEnumerable<ICommandHandler> commandHandlers)
        {
            DecoratedDispatcher = dispatcher;
            var defaultHandlerMatcher = new DefaultHandlerMatcher(commandHandlers);
            var commandDispatcherConfig = new CommandDispatcherConfig();    
            _dispatcher = new Dispatcher<ICommand>(defaultHandlerMatcher, commandDispatcherConfig);
        }

        public IDispatcher<ICommand> DecoratedDispatcher { get; }

        public async Task Dispatch(ICommand commandOrEvent)
        {
            await DecoratedDispatcher.Dispatch(commandOrEvent);
            await _dispatcher.Dispatch(commandOrEvent);
        }
    }
}