using System.Threading.Tasks;

namespace Avenue.Commands
{
    public interface ICommandValidator
    {
    }

    public interface ICommandValidator<in TCommand> : ICommandValidator
    {
        Task Validate(TCommand command);
    }
}