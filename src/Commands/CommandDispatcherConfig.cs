using Avenue.Dispatch;

namespace Avenue.Commands
{
    public class CommandDispatcherConfig : IDispatcherConfig
    {
        public int MaximumHandlersPerDispatch => 1;
        public int MinimumHandlersPerDispatch => 1;
    }
}