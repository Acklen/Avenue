using System.Threading.Tasks;
using Avenue.Dispatch;

namespace Avenue.Commands
{
    public interface ICommandHandler : IHandler
    {}

    public interface ICommandHandler<in TCommand> : ICommandHandler
    {
        Task Handle(TCommand command);
    }
}