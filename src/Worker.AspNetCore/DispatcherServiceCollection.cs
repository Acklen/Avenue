using Avenue.Worker;
using Microsoft.Extensions.DependencyInjection;

namespace Worker.AspNetCore
{
    public class DispatcherServiceCollection : ServiceCollection, IDispatcherServiceCollection
    {
    }
}