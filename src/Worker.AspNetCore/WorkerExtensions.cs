﻿using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.Worker;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Worker.AspNetCore
{
    public static class WorkerExtensions
    {
        public static IServiceCollection AddServer(this IServiceCollection services,
            Action<IServiceCollection> configAction, Func<IServiceProvider, Task> onStart)
        {
            var serviceCollection = new ServiceCollection();
            configAction.Invoke(serviceCollection);
            services.AddSingleton<IHostedService>(new ServerHostedService(serviceCollection, onStart));
            return services;
        }

        public static IServiceCollection AddServer(this IServiceCollection services,
            IServiceCollection workerServices, Func<IServiceProvider, Task> onStart)
        {
            services.AddSingleton<IHostedService>(new ServerHostedService(workerServices, onStart));
            return services;
        }

        public static IServiceCollection AddDispatchServer<T>(this IServiceCollection hostServices,
            ISubscriber<T> subscriber, Action<DispatchWorkerConfig<T>> config)
        {
            var dispatchWorkerConfig = new DispatchWorkerConfig<T>();   
            config.Invoke(dispatchWorkerConfig);
            
            var dispatcherServiceCollection = new DispatcherServiceCollection();
            dispatcherServiceCollection.AddTransient<IDispatcher<T>, BaseDispatcher<T>>();
            
            var configuredServices = dispatchWorkerConfig.Apply(dispatcherServiceCollection);
            
            return hostServices.AddServer(configuredServices, async provider =>
            {
                foreach (var initialization in dispatchWorkerConfig.Initializations)
                {
                    initialization.Invoke(provider);
                }
                var dispatcher = provider.GetService<IDispatcher<T>>();
                await subscriber.Subscribe(async command => await dispatcher.Dispatch(command));
            });
        }
    }
}