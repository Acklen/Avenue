using System;
using System.Collections.Generic;
using Avenue.Dispatch;
using Avenue.Domain;
using Avenue.Worker;
using Microsoft.Extensions.DependencyInjection;

namespace Worker.AspNetCore
{
    public class DispatchWorkerConfig<T>
    {
        readonly List<Action<IDispatcherServiceCollection>> _configs;
        readonly List<Action<IServiceProvider>> _initializations;

        public DispatchWorkerConfig()
        {
            _configs = new List<Action<IDispatcherServiceCollection>>();
            _initializations = new List<Action<IServiceProvider>>();
        }

        public IServiceCollection Apply(IDispatcherServiceCollection services)
        {
            _configs.ForEach(config => config.Invoke(services));
            return services;
        }

        public DispatchWorkerConfig<T> WithDependencies(Action<IDispatcherServiceCollection> config)
        {
            _configs.Add(config);
            return this;
        }

        public DispatchWorkerConfig<T> WithInitialization(Action<IServiceProvider> providerAction)
        {
            _initializations.Add(providerAction);
            return this;
        }

        public IEnumerable<Action<IServiceProvider>> Initializations => _initializations;

        public DispatchWorkerConfig<T> WithLogging<TLogger>()
        {
            _configs.Add(x => x
                    .Decorate(typeof(IDispatcher<T>), typeof(LoggedDispatcherDecorator<T>))
                    .AddTransient(typeof(IDispatcherLogger), typeof(TLogger))
                    .AddTransient<ITimeProvider, UtcTimeProvider>());
            
            return this;
        }
    }
}