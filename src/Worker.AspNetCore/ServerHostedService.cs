using System;
using System.Threading;
using System.Threading.Tasks;
using Avenue.Dispatch;
using Avenue.Worker;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Worker.AspNetCore
{
    public class ServerHostedService : IHostedService, IAvenueServer
    {
        readonly Func<IServiceProvider, Task> _onStart;
        readonly Func<IServiceProvider, Task> _onStop;
        readonly ServiceProvider _serviceProvider;
        bool _started = false;

        public ServerHostedService(IServiceCollection serviceCollection, Func<IServiceProvider,Task> onStart, Func<IServiceProvider,Task> onStop = null)
        {
            _onStart = onStart;
            _onStop = onStop;
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            VerifyNotStarted();
            var task = _onStart.Invoke(_serviceProvider);
            _started = true;
            return task;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            VerifyStarted();
            var task = _onStop == null ? Task.CompletedTask : _onStop.Invoke(_serviceProvider);
            _started = false;
            return task;
        }
        
        void VerifyNotStarted()
        {
            if (_started)
                throw new Exception($"The service was already started. It cannot be started again.");
        }

        void VerifyStarted()
        {
            if (!_started) throw new Exception($"The service was never started.");
        }
        
    }
}