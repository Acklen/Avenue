﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace Avenue.AspNetCore.Extensions
{
    public static class AspNetCoreExtensions
    {
        public static IServiceCollection Scan<T>(this IServiceCollection services, Assembly[] assemblies)
        {
            services.Scan(t => t
                .FromAssemblies(assemblies)
                .AddClasses(c => c.AssignableTo(typeof(T)))
                .AsSelfWithInterfaces()
                .WithTransientLifetime());

            return services;
        }
        
        public static IServiceCollection AddForEachType(this IServiceCollection services, Type serviceTypeOpen, Type implementationTypeOpen, IEnumerable<Type> possibleGenericTypeArguments, ServiceLifetime serviceLifetime = ServiceLifetime.Transient)
        {
            return services.DoForEachType(serviceTypeOpen, implementationTypeOpen, possibleGenericTypeArguments,
                (serviceType, implType) =>
                {
                    services.Add(new ServiceDescriptor(serviceType, implType, serviceLifetime));    
                });
        }

        public static IServiceCollection DecorateForEachType(this IServiceCollection services, Type serviceTypeOpen, Type implementationTypeOpen, IEnumerable<Type> possibleGenericTypeArguments)
        {
            return services.DoForEachType(serviceTypeOpen, implementationTypeOpen, possibleGenericTypeArguments,
                (serviceType, implType) =>
                {
                    services.Decorate(serviceType, implType);    
                });
        }

        public static IServiceCollection DoForEachType(this IServiceCollection services, Type serviceTypeOpen,
            Type implementationTypeOpen, IEnumerable<Type> possibleGenericTypeArguments, Action<Type, Type> doForEach)
        {
            foreach (var entityType in possibleGenericTypeArguments)
            {
                var serviceType = serviceTypeOpen.MakeGenericType(entityType);
                var implType = implementationTypeOpen.MakeGenericType(entityType);
                doForEach.Invoke(serviceType, implType);
            }

            return services;
        }

    }
}